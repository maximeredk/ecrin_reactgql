import Clients from './clients';

export default {
    Query : {
        clients(obj, args){
            return Clients.find({}).fetch() || {};
        },
        client(obj, {_id}){
            return Clients.findOne({_id}) || {};
        }
    },
    Mutation:{
        addClient(obj, {firstname,lastname,avatar,phone,mail,address,zipcode,city},{user}){
            if(user._id){
                const clientId = Clients.insert({
                    firstname:firstname,
                    lastname:lastname,
                    avatar:avatar,
                    phone:phone,
                    mail:mail,
                    address:address,
                    zipcode:zipcode,
                    city:city
                });
                return Clients.findOne(clientId);
            }
            throw new Error('Unauthorized');
        },
        editClient(obj, {_id,firstname,lastname,avatar,phone,mail,address,zipcode,city},{user}){
            if(user._id){
                Clients.update(
                    {
                        _id: _id
                    }, {
                        $set: {
                            "firstname":firstname,
                            "lastname":lastname,
                            "avatar":avatar,
                            "phone":phone,
                            "mail":mail,
                            "address":address,
                            "zipcode":zipcode,
                            "city":city
                        }
                    }
                );
                return Clients.findOne(_id);
            }
            throw new Error('Unauthorized');
        },
        removeClient(obj, {_id}){
            const res = Clients.remove(_id);
            return res;
        }
    }
}