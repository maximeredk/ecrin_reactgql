import VIngredients from './vingredients';
import Ingredients from '../ingredients/ingredients';

export default {
    Query : {
        vingredientsByVariation(obj, {_id}){
            const vibv = VIngredients.find({variation:_id}).fetch() || {};
            vibv.vingredients.forEach((vi,i) => {
                const ing = Ingredients.findOne({_id:vi.ingredient});
                vibv.vingredients[i].ingredient = ing;
            });
            return vibv;
        }
    },
    Mutation:{
        addVIngredientToVariation(obj, {variation,ingredient},{user}){
            if(user._id){
                const vi = VIngredients.find({variation:variation}).fetch();
                let same = false;
                vi.forEach(i => {
                    if(i.ingredient == ingredient){
                        same = true;
                    }
                });
                if(!same){
                    const vingredientId = VIngredients.insert({
                        usedQ:0,
                        usedU:Ingredients.findOne(ingredient).unite,
                        usedP:0,
                        variation:variation,
                        ingredient:ingredient
                    });
                    return (VIngredients.findOne(vingredientId).ingredient == ingredient);
                }
                return null;
            }
            throw new Error('Unauthorized');
        },
        deleteVIngredient(obj,{_id}){
            return VIngredients.remove(_id);
        },
        editVIngredient(obj,{_id,usedQ,usedU,usedP},{user}){
            if(user._id){
                return VIngredients.update(
                    {
                        _id: _id
                    }, {
                        $set: {
                            "usedQ":usedQ,
                            "usedU":usedU,
                            "usedP":usedP
                        }
                    }
                );
            }
            throw new Error('Unauthorized');
        }
    }
}
