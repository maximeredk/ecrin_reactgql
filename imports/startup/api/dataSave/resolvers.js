import Ingredients from '../ingredients/ingredients';
import VIngredients from '../vingredients/vingredients';
import {Recettes} from '../recettes/recettes';
import Variations from '../variations/variations';
import Clients from '../clients/clients';
import Prestations from '../prestations/prestations';
import Tasks from '../tasks/tasks';

export default {
    Query : {
        saveDB(obj, args){
            let DB = {};
            DB.ingredients = Ingredients.find({}).fetch() || {};
            DB.vIngredients = VIngredients.find({}).fetch() || {};
            DB.vIngredients.map(vI=>{
                vI.ingredient = Ingredients.findOne(vI.ingredient);
            })
            DB.recettes = Recettes.find({}).fetch() || {};
            DB.recettes.map(r=>{
                r.variations = Variations.find({recette:r._id});
            })
            DB.variations = Variations.find({}).fetch() || {};
            DB.variations.map(v=>{
                v.vingredients = VIngredients.find({variation:v._id});
            })
            DB.clients = Clients.find({}).fetch() || {};
            DB.prestations = Prestations.find({}).fetch() || {};
            DB.prestations.map(p=>{
                p.client = Clients.findOne(p.client);
            })
            DB.tasks = Tasks.find({}).fetch() || {};
            return DB;
        }
    }
}