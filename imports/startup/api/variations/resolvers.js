import Variations from './variations';
import VIngredients from '../vingredients/vingredients';
import VRecettes from '../vrecettes/vrecettes';
import Ingredients from '../ingredients/ingredients';

export default {
    Query : {
        variations(obj, args){
            return Variations.find({}).fetch() || {};
        },
        variationsByRecette(obj, {recette}){
            return Variations.find({recette:recette}).fetch() || {};
        },
        variation(obj, {_id}){
            const v = Variations.findOne({_id}) || {};
            v.vingredients = VIngredients.find({variation:_id}).fetch();
            v.vingredients.forEach((vi,i) => {
                const ing = Ingredients.findOne({_id:vi.ingredient});
                v.vingredients[i].ingredient = ing;
            });
            v.vrecettes = VRecettes.find({variation:_id}).fetch();
            v.vrecettes.forEach((vi,i) => {
                const ing = Ingredients.findOne({_id:vi.ingredient});
                v.vrecettes[i].ingredient = ing;
            });
            return v;
        }
    },
    Mutation:{
        addVariation(obj, {name,recette},{user}){
            if(user._id){
                const variationId = Variations.insert({
                    name:name,
                    recette:recette
                });
                return Variations.findOne(variationId);
            }
            throw new Error('Unauthorized');
        },
        removeVariation(obj, {_id}){
            if(Variations.find().fetch().length > 1){
                Variations.remove(_id);
                VIngredients.remove({variation:_id});
                VRecettes.remove({variation:_id});
                Ingredients.remove({fromVariation:_id});
            }
        }
    }
}
