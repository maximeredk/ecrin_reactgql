import { Recettes } from './recettes';
import VIngredients from '../vingredients/vingredients';
import Ingredients from '../ingredients/ingredients';
import Variations from '../variations/variations';
import _ from 'lodash';

export default {
    Query : {
        recettes(obj, args){
            return Recettes.find({}).fetch() || {};
        },
        recette(obj, {_id}){
            let r = Recettes.findOne({_id}) || {};
            r.variations = Variations.find({recette:_id}).fetch()
            return r;
        }
    },
    Mutation:{
        addRecette(obj, {name},{user}){
            if(user._id){
                const recetteId = Recettes.insert({
                    name:name,
                    isIngredient:false
                });
                Variations.insert({
                    name:"Basic",
                    recette:recetteId,
                    allergenics:[false,false,false,false,false,false,false,false,false,false,false,false,false,false]
                });
                return Recettes.findOne(recetteId);
            }
            throw new Error('Unauthorized');
        },
        removeRecette(obj, {_id}){
            const res = Recettes.remove(_id);
            const variations = Variations.find({recette:_id})
            variations.forEach(v=>{
                Variations.remove(v._id);
                VIngredients.remove({variation:v._id});
                VRecettes.remove({variation:v._id});
                Ingredients.remove({fromVariation:v._id});
            });
            return res;
        },
        setIngredient(obj, {_id,isIngredient}){
            Recettes.update(
                {
                    _id: _id
                }, {
                    $set: {
                        "isIngredient":isIngredient
                    }
                }
            );
            if(isIngredient){
                const r = Recettes.findOne({_id});
                const vs = Variations.find({recette:_id}).fetch();
                vs.forEach(v=>{
                    const vis = VIngredients.find({variation:v._id}).fetch();
                    Ingredients.insert({
                        name:r.name +" ["+ v.name +"]",
                        quantite:1,
                        unite:"unité",
                        prix:parseFloat(_.sumBy(vis,"usedP")),
                        fromVariation:v._id
                    });
                });
            }else{
                const vs = Variations.find({recette:_id}).fetch();
                vs.forEach(v=>{
                    Ingredients.remove({fromVariation:v._id});
                    VRecettes.remove({variation:v._id});
                });
            }
            return Recettes.findOne(_id);
        }
    }
}
