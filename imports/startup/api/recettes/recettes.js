import { Mongo } from 'meteor/mongo';

export const Recettes = new Mongo.Collection("recettes");
export const Allergenics = new Mongo.Collection("allergenics");