import Tasks from '../tasks/tasks';

export default {
    Query : {
        tasks(obj, args){
            return Tasks.find({}).fetch() || {};
        }
    },
    Mutation:{
        addTask(obj, {content},{user}){
            if(user._id){
                Tasks.insert({
                    content:content,
                    completed:false
                });
                return Tasks.find({}).fetch() || {};
            }
            throw new Error('Unauthorized');
        },
        deleteTask(obj, {_id}){
            Tasks.remove(_id);
            return Tasks.find({}).fetch() || {};
        },
        completeTask(obj, {_id}){
            const task = Tasks.findOne(_id);
            Tasks.update(
                {
                    _id: _id
                }, {
                    $set: {
                        "completed":!task.completed
                    }
                }
            );
            return Tasks.findOne(_id);
        }
    }
}
