export default {
    Query : {
        user(obj, args, { user }){
            return user || {}
        },
        users(obj, args){
            return Meteor.users.find({}).fetch() || {};
        }
    },
    User:{
        email:user => (user._id != null ? user.emails[0].address : null),
        firstname:user=> (user._id != null ? user.profile.firstname : null),
        lastname:user=> (user._id != null ? user.profile.lastname : null),
        isAdmin:user=> (user._id != null ? user.settings.isAdmin : null),
        activated:user=> (user._id != null ? user.settings.activated : null)
    },
    Mutation:{
        editUserProfile(obj, {_id,email,firstName,lastName,age}){
            const res = Meteor.users.update(
                {
                    _id: _id
                }, {
                    $set: {
                        "emails[0].address": email,
                        "profile.firstName": firstName,
                        "profile.lastName": lastName,
                        "profile.age": age
                    }
                }
            );
            return res;
        },
        toggleAdmin(obj, {_id}){
            const user = Meteor.users.findOne({_id:_id});
            Meteor.users.update(
                {
                    _id: _id
                }, {
                    $set: {
                        "settings.isAdmin": !user.settings.isAdmin,
                    }
                }
            );
            const res = Meteor.users.findOne({_id:_id});
            return res;
        },
        removeUser(obj, {_id}){
            const res = Meteor.users.remove(_id);
            return res;
        },
        toggleActive(obj, {admin,_id},{user}){
            if(user._id){
                const adminUser = Meteor.users.findOne({_id:admin});
                if(adminUser.settings.isAdmin){
                    const user = Meteor.users.findOne({_id:_id});
                    Meteor.users.update({
                        _id: _id
                    }, {
                        $set: {
                            "settings.activated": !user.settings.activated,
                        }
                    });
                }
                const res = Meteor.users.findOne({_id:_id});
                return res;
            }
            throw new Error('Unauthorized')
        },
        deleteAccount(obj, {admin,_id},{user}){
            if(user._id){
                const adminUser = Meteor.users.findOne({_id:admin});
                if(adminUser.settings.isAdmin){
                    Meteor.users.remove(_id);
                    return Meteor.users.find({}).fetch() || {};
                }
            }
            throw new Error('Unauthorized')
        }
    }
}