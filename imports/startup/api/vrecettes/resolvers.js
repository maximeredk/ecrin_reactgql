import VRecettes from './vrecettes';
import Ingredients from '../ingredients/ingredients';

export default {
    Query : {
        vrecettesByVariation(obj, {_id}){
            const vibv = VRecettes.find({variation:_id}).fetch() || {};
            vibv.vingredients.forEach((vi,i) => {
                const ing = Ingredients.findOne({_id:vi.ingredient});
                vibv.vingredients[i].ingredient = ing;
            });
            return vibv;
        }
    },
    Mutation:{
        addVRecetteToVariation(obj, {variation,ingredient},{user}){
            if(user._id){
                const vi = VRecettes.find({variation:variation}).fetch();
                let same = false;
                vi.forEach(i => {
                    if(i.ingredient == ingredient){
                        same = true;
                    }
                });
                if(!same){
                    const vingredientId = VRecettes.insert({
                        usedQ:0,
                        usedU:Ingredients.findOne(ingredient).unite,
                        usedP:0,
                        variation:variation,
                        ingredient:ingredient
                    });
                    return (VRecettes.findOne(vingredientId).ingredient == ingredient);
                }
                return null;
            }
            throw new Error('Unauthorized');
        },
        deleteVRecette(obj,{_id}){
            return VRecettes.remove(_id);
        },
        editVRecette(obj,{_id,usedQ,usedU,usedP},{user}){
            if(user._id){
                return VRecettes.update(
                    {
                        _id: _id
                    }, {
                        $set: {
                            "usedQ":usedQ,
                            "usedU":usedU,
                            "usedP":usedP
                        }
                    }
                );
            }
            throw new Error('Unauthorized');
        }
    }
}
