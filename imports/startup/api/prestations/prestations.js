import { Mongo } from 'meteor/mongo';

const Prestations = new Mongo.Collection("prestations");

export default Prestations;
