import Prestations from './prestations';
import Clients from '../clients/clients';

export default {
    Query : {
        prestations(obj, args){
            return Prestations.find({}).fetch() || {};
        },
        prestation(obj, {_id}){
            const prestation = Prestations.findOne({_id}) || {};
            prestation.client = Clients.findOne({_id:prestation.client}) || {};
            return prestation;
        },
        getPrestationsByDays(obj,{year,month}){
            let prestationsByDays = [];
            const prestations = Prestations.find({deliveryYear:year,deliveryMonth:month}).fetch()
            for (var i = 1; i <= 31; i++) {
                prestationsByDays[i-1] = prestations.filter(p => p.deliveryDay == i);
            }
            return prestationsByDays;
        },
        getPrestationsOfDay(obj,{day,month,year}){
            let prestations = Prestations.find({deliveryYear:year,deliveryMonth:month-1,deliveryDay:day}).fetch()
            prestations.forEach((pre,i) => {
                prestations[i].client = Clients.findOne({_id:pre.client});
            });
            return prestations;
        }
    },
    Mutation:{
        addPrestation(obj, {name,client,deliveryDay,deliveryMonth,deliveryYear,facturation,paid,archived},{user}){
            if(user._id){
                const prestationId = Prestations.insert({
                    name:name,
                    client:client,
                    deliveryDay:deliveryDay,
                    deliveryMonth:deliveryMonth,
                    deliveryYear:deliveryYear,
                    description:"",
                    facturation:facturation,
                    paid:paid,
                    archived:archived,
                    step:0
                });
                return Prestations.findOne(prestationId);
            }
            throw new Error('Unauthorized');
        },
        removePrestation(obj, {_id}){
            const res = Prestations.remove(_id);
            return res;
        },
        setStep(obj, {_id,step},{user}){
            if(user._id){
                Prestations.update(
                    {
                        _id: _id
                    }, {
                        $set: {
                            "step":step
                        }
                    }
                );
                return Prestations.findOne({_id:_id})
            }
            throw new Error('Unauthorized');
        },
        setPaid(obj, {_id,paid},{user}){
            if(user._id){
                Prestations.update(
                    {
                        _id: _id
                    }, {
                        $set: {
                            "paid":paid
                        }
                    }
                );
                return Prestations.findOne({_id:_id})
            }
            throw new Error('Unauthorized');
        },
        setFacturation(obj, {_id,facturation},{user}){
            if(user._id){
                Prestations.update(
                    {
                        _id: _id
                    }, {
                        $set: {
                            "facturation":facturation
                        }
                    }
                );
                return Prestations.findOne({_id:_id})
            }
            throw new Error('Unauthorized');
        },
        setDescription(obj, {_id,description},{user}){
            if(user._id){
                Prestations.update(
                    {
                        _id: _id
                    }, {
                        $set: {
                            "description":description
                        }
                    }
                );
                return Prestations.findOne({_id:_id})
            }
            throw new Error('Unauthorized');
        } 
    }
}