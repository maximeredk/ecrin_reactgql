import Ingredients from './ingredients';
import VIngredients from '../vingredients/vingredients';

export default {
    Query : {
        ingredients(obj, args){
            return Ingredients.find({fromVariation:null}).fetch() || {};
        },
        ingredientsFromVariation(obj, args){
            return Ingredients.find({fromVariation:{$ne:null}}).fetch() || {};
        }
    },
    Mutation:{
        addIngredient(obj, {name,prix,quantite,unite,fournisseur},{user}){
            if(user._id){
                Ingredients.insert({
                    name:name,
                    prix:prix,
                    quantite:quantite,
                    unite:unite,
                    fromVariation:null,
                    fournisseur:fournisseur,
                    allergenics:[false,false,false,false,false,false,false,false,false,false,false,false,false,false]
                });
                return Ingredients.find({fromVariation:null}).fetch() || {};
            }
            throw new Error('Unauthorized');
        },
        editIngredient(obj, {_id,name,prix,quantite,unite,fournisseur},{user}){
            if(user._id){
                Ingredients.update(
                    {
                        _id: _id
                    }, {
                        $set: {
                            "name":name,
                            "prix":prix,
                            "quantite":quantite,
                            "unite":unite,
                            "fournisseur":fournisseur
                        }
                    }
                );
                VIngredients.find({ingredient:_id}).forEach(vi=>{
                    let fac = 0;
                    if(unite=="g" && vi.usedU=="mL"){fac = 1;}
                    if(unite=="mL" && vi.usedU=="g"){fac = 1;}
                    if(unite=="Kg" && vi.usedU=="L"){fac = 1;}
                    if(unite=="L" && vi.usedU=="Kg"){fac = 1;}
                    if(unite=="mL" && vi.usedU=="Kg"){fac = 1000;}
                    if(unite=="Kg" && vi.usedU=="mL"){fac = 0.001;}
                    if(unite=="L" && vi.usedU=="g"){fac = 0.001;}
                    if(unite=="g" && vi.usedU=="L"){fac = 1000;}
                    if(unite=="g" && vi.usedU=="g"){fac = 1;}
                    if(unite=="g" && vi.usedU=="Kg"){fac = 1000;}
                    if(unite=="Kg" && vi.usedU=="g"){fac = 0.001;}
                    if(unite=="Kg" && vi.usedU=="Kg"){fac = 1;}
                    if(unite=="mL" && vi.usedU=="L"){fac = 1000;}
                    if(unite=="mL" && vi.usedU=="mL"){fac = 1;}
                    if(unite=="L" && vi.usedU=="L"){fac = 1;}
                    if(unite=="L" && vi.usedU=="mL"){fac = 0.001;}
                    if(unite=="cm" && vi.usedU=="cm"){fac = 1;}
                    if(unite=="cm" && vi.usedU=="m"){fac = 100;}
                    if(unite=="m" && vi.usedU=="m"){fac = 1;}
                    if(unite=="m" && vi.usedU=="cm"){fac = 0.01;}
                    if(unite=="unité" && vi.usedU=="unité"){fac = 1;}
                    const usedP = (vi.usedQ/(quantite/fac))*prix;
                    VIngredients.update(
                        {
                            _id: vi._id
                        }, {
                            $set: {
                                "usedP":usedP
                            }
                        }
                    );
                })
                return Ingredients.findOne(_id);
            }
            throw new Error('Unauthorized');
        },
        removeIngredient(obj, {_id}){
            Ingredients.remove(_id);
            VIngredients.remove({ingredient:_id});
            return Ingredients.find({fromVariation:null}).fetch() || {};
        },
        setAllergenics(obj, {_id,allergenics},{user}){
            if(user._id){
                Ingredients.update(
                    {
                        _id: _id
                    }, {
                        $set: {
                            "allergenics":allergenics
                        }
                    }
                );
                return Ingredients.find({fromVariation:null}).fetch() || {};
            }
            throw new Error('Unauthorized');
        },
        reset(obj,args,{user}){
            Ingredients.remove({});
            VIngredients.remove({});
            return true;
        },
        addProp(obj,args,{user}){
            Ingredients.update(
                {}, {
                    $set: {
                        "fournisseur":""
                    }
                },{multi: true}
            );
            return true;
        }
    }
}