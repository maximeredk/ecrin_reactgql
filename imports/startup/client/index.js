import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { ApolloLink, from } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { BrowserRouter } from 'react-router-dom';
import { MeteorAccountsLink } from 'meteor/apollo'

import { UserProvider } from '../../contexts/UserContext';
import { ClientsProvider } from '../../contexts/ClientsContext';
import { PrestationsProvider } from '../../contexts/PrestationsContext';
import { CalendarProvider } from '../../contexts/CalendarContext';

import App from '../../ui/App';

const client = new ApolloClient({
    link: ApolloLink.from([
      new MeteorAccountsLink({ headerName: 'meteor-login-token' }),
      new HttpLink({
        uri: '/graphql'
      })
    ]),
    cache: new InMemoryCache()
})

const ApolloApp = () => (
    <ApolloProvider client={client}>
        <BrowserRouter>
            <UserProvider client={client}>
                <ClientsProvider>
                    <PrestationsProvider>
                        <App/>
                    </PrestationsProvider>
                </ClientsProvider>
            </UserProvider>
        </BrowserRouter>
    </ApolloProvider>
)

Meteor.startup(()=>{
    render(<ApolloApp />, document.getElementById("app"));
});