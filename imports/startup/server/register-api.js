
import { ApolloServer, gql } from 'apollo-server-express'
import { WebApp } from 'meteor/webapp'
import { getUser } from 'meteor/apollo'
import merge from 'lodash/merge';

import IngredientSchema from '../api/ingredients/Ingredient.graphql';
import IngredientResolvers from '../api/ingredients/resolvers.js';
import UserSchema from '../api/user/User.graphql';
import UserResolvers from '../api/user/resolvers.js';
import RecettesSchema from '../api/recettes/Recette.graphql';
import RecettesResolvers from '../api/recettes/resolvers.js';
import ClientsSchema from '../api/clients/Client.graphql';
import ClientsResolvers from '../api/clients/resolvers.js';
import PrestationsSchema from '../api/prestations/Prestation.graphql';
import PrestationsResolvers from '../api/prestations/resolvers.js';
import CalendarSchema from '../api/calendar/Calendar.graphql';
import CalendarResolvers from '../api/calendar/resolvers.js';
import VariationsSchema from '../api/variations/Variation.graphql';
import VariationsResolvers from '../api/variations/resolvers.js';
import VIngredientsSchema from '../api/vingredients/VIngredient.graphql';
import VIngredientsResolvers from '../api/vingredients/resolvers.js';
import VRecettesSchema from '../api/vrecettes/VRecette.graphql';
import VRecettesResolvers from '../api/vrecettes/resolvers.js';
import TasksSchema from '../api/tasks/Task.graphql';
import TasksResolvers from '../api/tasks/resolvers.js';
import DataSaveSchema from '../api/dataSave/DataSave.graphql';
import DataSaveResolvers from '../api/dataSave/resolvers.js';

//ssccs

const typeDefs = [
    IngredientSchema,
    UserSchema,
    RecettesSchema,
    ClientsSchema,
    PrestationsSchema,
    CalendarSchema,
    VariationsSchema,
    VIngredientsSchema,
    VRecettesSchema,
    TasksSchema,
    DataSaveSchema
];

const resolvers = merge(
    IngredientResolvers,
    UserResolvers,
    RecettesResolvers,
    ClientsResolvers,
    PrestationsResolvers,
    CalendarResolvers,
    VariationsResolvers,
    VIngredientsResolvers,
    VRecettesResolvers,
    TasksResolvers,
    DataSaveResolvers
);

//0108

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: async ({ req }) => {
        const token = req.headers["meteor-login-token"] || '';
        const user = await getUser(token);
        return { user };
    }
})

const corsOptions = {
    origin: 'http://localhost:3000',
    credentials: true
}

server.applyMiddleware({
    app: WebApp.connectHandlers,
    path: '/graphql',
    cors: corsOptions
})

WebApp.rawConnectHandlers.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Authorization,Content-Type");
    return next();
});
  
WebApp.connectHandlers.use('/graphql', (req, res) => {
    if (req.method === 'GET') {
        res.end()
    }
})