import React, { Component } from 'react'
import gql from 'graphql-tag';
import { graphql, withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';


export const PrestationsContext = React.createContext();

const PrestationsQuery = gql`
    query Prestations{
        prestations{
            _id
            name
            client{
                _id
                firstname
                lastname
                avatar
                phone
                mail
                address
                zipcode
                city
            }
            deliveryDay
            deliveryMonth
            deliveryYear
            facturation
            paid
            archived
        }
    }
`;

class Provider extends Component {
    render(){
        return (
            <PrestationsContext.Provider value={{
                ...this.state,
                prestations:this.props.prestations
            }}>
                {this.props.children}
            </PrestationsContext.Provider>
        );
    }
}

export const PrestationsProvider =
    graphql(PrestationsQuery,{
        props:({data}) =>({...data})
})(withApollo(
    Provider
));