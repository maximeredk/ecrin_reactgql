import React, { Component } from 'react'
import gql from 'graphql-tag';
import { graphql, withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';

export const UserContext = React.createContext();

const UserQuery = gql`
    query User{
        user{
            _id
            email
            isAdmin
            activated
        }
        users{
            _id
            email
            isAdmin
            firstname
            lastname
            activated
        }
    }
`;

class Provider extends Component {

    state={
        allergenics:[
            {
                id:1,
                name:"almond",
                label:"Amande"
            },{
                id:2,
                name:"celeri",
                label:"Celeri"
            },{
                id:3,
                name:"crusta",
                label:"Custacé"
            },{
                id:4,
                name:"egg",
                label:"Oeufs"
            },{
                id:5,
                name:"fish",
                label:"Poisson"
            },{
                id:6,
                name:"gluten",
                label:"Gluten"
            },{
                id:7,
                name:"lupin",
                label:"Lupin"
            },{
                id:8,
                name:"milk",
                label:"Lait"
            },{
                id:9,
                name:"mollus",
                label:"Mollusque"
            },{
                id:10,
                name:"mustard",
                label:"Moutarde"
            },{
                id:11,
                name:"peanut",
                label:"Ararchides"
            },{
                id:12,
                name:"sesame",
                label:"Sesame"
            },{
                id:13,
                name:"soja",
                label:"Soja"
            },{
                id:14,
                name:"sulfite",
                label:"Sulfites"
            }
        ],
        navigateToRecette: id => {
            this.props.history.push("/recette/"+id);
        },
        getRecetteById: id => {
            var index = this.state.reci.map(x => {
                return parseInt(x.id);
            }).indexOf(parseInt(id));
            return this.state.reci[index];
        },
        navigateToPrestation: id => {
            this.props.history.push("/prestation/"+id);
        },
        fullNameAllergenics:[
            {original:"almond",full:"Fruit à coques"},
            {original:"celeri",full:"Céleri"},
            {original:"crusta",full:"Crustacé"},
            {original:"egg",full:"Oeufs"},
            {original:"fish",full:"Poisson"},
            {original:"gluten",full:"Gluten"},
            {original:"lupin",full:"Lupin"},
            {original:"milk",full:"Lait"},
            {original:"mollus",full:"Mollusque"},
            {original:"mustard",full:"Moutarde"},
            {original:"peanut",full:"Arachides"},
            {original:"sesame",full:"Graine de sésame"},
            {original:"soja",full:"Soja"},
            {original:"sulfite",full:"Anhydride sulfureux et sulfites"}
        ]
    }

    getFullName = original => {
        return this.state.fullNameAllergenics.find(x=>original == x.original).full
    }

    toast = ({message,type}) => {
        if(type == 'error'){
            toast.error(message);
        }
        if(type == 'success'){
            toast.success(message);
        }
        if(type == 'info'){
            toast.info(message);
        }
        if(type == 'warning'){
            toast.warn(message);
        }
    }

    render(){
        return (
            <UserContext.Provider value={{
                ...this.state,
                user: (this.props.user == null ? {_id:null} : this.props.user),
                users: this.props.users,
                client : this.props.client,
                toast:this.toast,
                getFullName:this.getFullName
            }}>
                {this.props.children}
            </UserContext.Provider>
        );
    }
}

export const UserProvider =
    graphql(UserQuery,{
        props:({data}) =>({...data})
})(withApollo(
    withRouter(Provider)
));