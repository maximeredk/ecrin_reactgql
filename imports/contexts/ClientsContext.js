import React, { Component } from 'react'
import gql from 'graphql-tag';
import { graphql, withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';

export const ClientsContext = React.createContext();

const ClientsQuery = gql`
    query Clients{
        clients{
            _id
            firstname
            lastname
            avatar
            phone
            mail
            address
            zipcode
            city
        }
    }
`;

class Provider extends Component {

    state={
        navigateToClient: _id => {
            this.props.history.push("/client/"+_id);
        }
    }

    render(){
        return (
            <ClientsContext.Provider value={{
                ...this.state,
                clients:this.props.clients
            }}>
                {this.props.children}
            </ClientsContext.Provider>
        );
    }
}

export const ClientsProvider = graphql(ClientsQuery,{
    props:({data}) =>({...data})
})(withApollo(
    withRouter(Provider)
));