import React, { Component } from 'react'
import gql from 'graphql-tag';
import { graphql, withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';


export const RecettesContext = React.createContext();

const RecettesQuery = gql`
    query Recettes{
        recettes{
            _id
            name
        }
    }
`;

class Provider extends Component {

    render(){
        return (
            <RecettesContext.Provider value={{
                ...this.state,
                recettes:this.props.recettes
            }}>
                {this.props.children}
            </RecettesContext.Provider>
        );
    }
}

export const RecettesProvider =
    graphql(RecettesQuery,{
        props:({data}) =>({...data})
})(withApollo(
    Provider
));