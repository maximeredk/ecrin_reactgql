import React, { Component } from 'react'
import gql from 'graphql-tag';
import { graphql, withApollo } from 'react-apollo';

export const IngredientsContext = React.createContext();

const IngredientsQuery = gql`
    query Ingredients{
        ingredients{
            _id
            name
            prix
            quantite
            unite
        }
    }
`;

class Provider extends Component {
    
    render(){
        return (
            <IngredientsContext.Provider value={{
                ...this.state,
                ingredients:this.props.ingredients
            }}>
                {this.props.children}
            </IngredientsContext.Provider>
        );
    }
}

export const IngredientsProvider =
    graphql(IngredientsQuery,{
        props:({data}) =>({...data})
})(withApollo(
    Provider
));