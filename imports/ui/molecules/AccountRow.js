import React, { Component, Fragment } from 'react'
import { Table, Label, Dropdown, Icon, Message, Button, Modal } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';

class AccountRow extends Component {

    state={
        user:this.props.u,
        openDelete:false,
        activated:this.props.u.activated,
        isAdmin:this.props.u.isAdmin,
        toggleAdminQuery:gql`mutation toggleAdmin($_id: String!){
            toggleAdmin(_id: $_id){
                _id
                isAdmin
            }
        }`,
        toggleActiveQuery:gql`mutation toggleActive($admin: String!,$_id: String!){
            toggleActive(admin: $admin,_id: $_id){
                _id
                activated
            }
        }`
    }

    closeDelete = () => {
        this.setState({openDelete:false})
    }

    showDelete = () => {
        this.setState({openDelete:true})
    }

    deleteAccount = _id => {
        this.closeDelete();
        this.props.deleteAccount(_id);
    }

    setAdmin = _id =>{
        this.props.client.mutate({
            mutation : this.state.toggleAdminQuery,
            variables:{
                _id:_id
            }
        }).then(({data})=>{
            this.setState({
                isAdmin:data.toggleAdmin.isAdmin
            });
        })
    }
    
    activateAccount = _id =>{
        this.props.client.mutate({
            mutation : this.state.toggleActiveQuery,
            variables:{
            admin:Meteor.userId(),
            _id:_id
            }
        }).then(({data})=>{
            this.setState({
                activated:data.toggleActive.activated
            });
        })
    }

    render() {
        const { user,activated,isAdmin } = this.state;
        return (
            <Fragment>
                <Table.Row>
                    <Table.Cell>{user.firstname + " " + user.lastname}</Table.Cell>
                    <Table.Cell>{user.email}</Table.Cell>
                    <Table.Cell style={{textAlign:"center"}}>
                        {(user.isOwner ?
                        <Label style={{textAlign:"center"}} color="blue">Propriétaire</Label>
                        :
                        (isAdmin ?
                            <Label style={{textAlign:"center"}} color="yellow">
                                oui
                                <Label.Detail>
                                    <Icon name='check'/>
                                </Label.Detail>
                            </Label>
                        :
                            <Label style={{textAlign:"center"}}>
                                non
                                <Label.Detail>
                                    <Icon name='delete'/>
                                </Label.Detail>
                            </Label>
                        )
                    )}
                    </Table.Cell>
                    <Table.Cell style={{textAlign:"center"}}>
                        {(activated ?
                            <Label style={{textAlign:"center"}}>
                                actif
                                <Label.Detail>
                                    <Icon name='check'/>
                                </Label.Detail>
                            </Label>
                        :
                            <Label style={{textAlign:"center"}} color="black">
                                inactif
                                <Label.Detail>
                                    <Icon name='delete'/>
                                </Label.Detail>
                            </Label>
                        )}
                    </Table.Cell>
                    <Table.Cell style={{textAlign:"center"}}>
                        {(Meteor.userId() != user._id && this.props.user.isAdmin ? 
                            <Dropdown style={{margin:"0",padding:"6px"}} text='Actions ...' floating labeled button className='icon'>
                            <Dropdown.Menu>
                                {(isAdmin ?
                                    <Dropdown.Item icon='delete' color="orange" text="Retirer les droits d'admin" onClick={()=>{this.setAdmin(user._id)}}/>
                                :
                                    <Dropdown.Item icon='certificate' color="gree" text="Donner les droits d'admin" onClick={()=>{this.setAdmin(user._id)}}/>
                                )}
                                {(
                                    (Meteor.userId() != user._id ? 
                                        (activated ?
                                            <Dropdown.Item icon='delete' color="orange" text='Désactiver le compte' onClick={()=>{this.activateAccount(user._id)}}/>
                                        :
                                            <Dropdown.Item icon='check' color="green" text='Activer le compte' onClick={()=>{this.activateAccount(user._id)}}/>
                                        )
                                    :
                                        ""
                                    )
                                )}
                                <Dropdown.Item icon='trash' color="red" text='Supprimer le compte' onClick={()=>{this.showDelete(user._id)}}/>
                            </Dropdown.Menu>
                            </Dropdown>
                        :"")}
                    </Table.Cell>
                </Table.Row>
                <Modal closeOnDimmerClick={false} dimmer="inverted" size="small" open={this.state.openDelete} onClose={this.closeDelete} closeIcon>
                    <Modal.Header>
                        Confirmez la suppression du compte:
                    </Modal.Header>
                    <Modal.Content style={{textAlign:"center"}}>
                        <Message color='red' icon>
                            <Icon name='warning sign'/>
                            <Message.Content style={{display:"grid",gridTemplateColumns:"1fr 2fr",gridTemplateRows:"1fr 1fr"}}>
                                <p style={{margin:"8px 4px",placeSelf:"center end"}}>Utilisateur :</p>
                                <p style={{margin:"8px 4px",fontWeight:"800",placeSelf:"center start"}}>
                                    {user.firstname + " " + user.lastname}
                                </p>
                                <p style={{margin:"8px 4px",placeSelf:"center end"}}>Mail :</p>
                                <p style={{margin:"8px 4px",fontWeight:"800",placeSelf:"center start"}}>
                                    {user.email}
                                </p>
                            </Message.Content>
                        </Message>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color="red" onClick={()=>{this.deleteAccount(user._id)}}>Supprimer</Button>
                    </Modal.Actions>
                </Modal>
            </Fragment>
        )
    }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
export default wrappedInUserContext = withUserContext(AccountRow);