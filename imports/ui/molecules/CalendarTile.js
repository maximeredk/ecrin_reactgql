import React, { Component, Fragment } from 'react';
import { Button, Icon, Modal, Dropdown, Label, Input } from 'semantic-ui-react';
import { PrestationsContext } from '../../contexts/PrestationsContext';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

class CalendarTile extends Component {
    state={
        date:new Date(this.props.day.year,this.props.day.month,this.props.day.day),
        day:this.props.day.day,
        month:this.props.day.month,
        year:this.props.day.year,
        dow:this.props.day.dow,
        today:this.props.day.today,
        prestations:this.props.day.prestations.filter(prestation => prestation.step < 4),
        totalFacturationDay:_.sumBy(this.props.day.prestations,"facturation"),
        totalPaidDay:_.sumBy(this.props.day.prestations,"paid"),
        selected:false,
        open:false,
        selectedClient:null,
        selectedName:""
    }

    show = () =>{
      this.setState({ open: true })
    }

    close = () => {
      this.setState({ open: false })
    }

    handleChange = e =>{
      this.setState({
        [e.target.name]:e.target.value
      });
    }

    handleClientSelection = (e,{value}) =>{
      this.setState({
        selectedClient:value
      });
    }

    addPrestation = () => {
      this.props.addPrestation({
        client:this.state.selectedClient,
        dDay:parseInt(this.state.day),
        dMonth:parseInt(this.state.month),
        dYear:parseInt(this.state.year),
        name:this.state.selectedName
      });
      this.close();
    }

    getDayName = n => {
        return ["","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"][n];
    }

    getMonthName = n =>{
      return ["Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre"][n];
    }

    selectDay = () =>{
      this.props.selectDate(this.state.year,this.state.month,this.state.day);
    }

  render() {
    let { dow,day,today,open,month,year,prestations } = this.state;
    let { selected,clientsMappedList } = this.props;
    let dayColorCode;
    switch (true) {
      case (prestations.length < 1):
        dayColorCode = "black";
        break;
      case (prestations.length < 2):
        dayColorCode = "green";
        break;
      case (prestations.length < 4):
        dayColorCode = "blue";
        break;
      case (prestations.length < 6):
        dayColorCode = "violet";
        break;
      case (prestations.length < 8):
        dayColorCode = "yellow";
        break;
      case (prestations.length >= 8):
        dayColorCode = "red";
        break;
      default:
        dayColorCode = "black";
        break;
    }
    let headerColor = "linear-gradient(315deg, #000000 0%, #414141 74%)";
    let bodyColor = "linear-gradient(315deg, #f3e6e8 0%, #d5d0e5 74%)";
    if(today == true){headerColor = "linear-gradient(315deg, #E0A235 0%, #E0861A 74%)";bodyColor = "linear-gradient(315deg, #ECC785 0%, #ECB675 74%)"}
    if(selected == true){
      headerColor = "linear-gradient(315deg, #2a2a72 0%, #009ffd 74%)";
      bodyColor = "linear-gradient(315deg, #000000 0%, #414141 74%)";

      return (
        <Fragment>
          <div onClick={()=>{this.selectDay()}} style={{cursor:"pointer",justifyItems:"stretch",display:"grid",gridTemplateRows:"32px 1fr",gridColumnStart:dow}}>
            <div style={{backgroundColor:"#000000",borderColor:"#414141",borderWidth:"3px 3px 3px 3px",borderStyle:"solid",backgroundImage:headerColor,borderRadius:"4px 4px 0 0",color:"#fff",display:"flex"}}>
              <p style={{margin:"auto"}}>{this.getDayName(dow).substring(0,3)+" "+('00'+day).slice(-2)}</p>
            </div>
            <div style={{display:"grid",gridTemplateRows:"26px 1fr 26px",gridTemplateColumns:"26px 1fr 26px",borderColor:"#414141",borderWidth:"0 3px 3px 3px",borderStyle:"solid",borderRadius:"0 0 4px 4px",backgroundColor:"#ecf0f1",backgroundImage:bodyColor}}>
              <Button style={{justifySelf:"center",alignSelf:"center",gridColumnStart:"2",gridRowStart:"2"}} onClick={this.show} size="mini" color="blue" animated='fade'>
                <Button.Content hidden>Créer</Button.Content>
                <Button.Content visible>
                  <Icon name='plus' />
                </Button.Content>
              </Button>
            </div>
          </div>
          <Modal style={{borderRadius:"16px"}} open={open} onClose={this.close}>
            <Modal.Header><p style={{margin:"auto"}}>Ajouter une prestation pour le {this.getDayName(dow)+" "+('00'+day).slice(-2)+" "+this.getMonthName(month)+" "+year} ?</p></Modal.Header>
            <Modal.Content style={{display:"flex",justifyContent:"center"}}>
              <Input placeholder='Nom' name='selectedName' width={4} onChange={this.handleChange}/>
              <Dropdown style={{margin:"0 16px",width:"300px"}} placeholder="Client" name="client" onChange={this.handleClientSelection} fluid search scrolling selection options={clientsMappedList} />
            </Modal.Content>
            <Modal.Actions>
              <Button color="blue" icon='checkmark' labelPosition='right' content="Créer la prestation" onClick={this.addPrestation}/>
            </Modal.Actions>
          </Modal>
        </Fragment>
      )
    }else{
      return (
        <div onClick={()=>{this.selectDay()}} style={{cursor:"pointer",justifyItems:"stretch",display:"grid",gridTemplateRows:"32px 1fr",gridColumnStart:dow}}>
          <div style={{backgroundColor:"#000000",borderColor:"#414141",borderWidth:"3px 3px 3px 3px",borderStyle:"solid",backgroundImage:headerColor,borderRadius:"4px 4px 0 0",color:"#fff",display:"flex"}}>
            <p style={{margin:"auto"}}>{this.getDayName(dow).substring(0,3)+" "+('00'+day).slice(-2)}</p>
          </div>
          <div style={{display:"grid",gridTemplateRows:"26px 1fr",gridTemplateColumns:"1fr",justifyContent:"center",alignItems:"center",borderColor:"#414141",borderWidth:"0 3px 3px 3px",borderStyle:"solid",borderRadius:"0 0 4px 4px",backgroundColor:"#ecf0f1",backgroundImage:bodyColor}}>
            <Label style={{margin:"3px 3px 0 0",justifySelf:"end",alignSelf:"start"}} color={dayColorCode}>
              {prestations.length}
            </Label>
          </div>
        </div>
      )
    }
  }
}

const withPrestationsContext = WrappedComponent => props => (
  <PrestationsContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </PrestationsContext.Consumer>
)

const wrappedPrestationContext = withPrestationsContext(CalendarTile);

export default withRouter(wrappedPrestationContext);