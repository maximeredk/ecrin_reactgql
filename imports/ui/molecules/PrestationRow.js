import React, { Component } from 'react';
import { Table, Button, Icon, Label } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';

export class PrestationRow extends Component {
    
    navigate = () => {
        this.props.navigateToPrestation(this.props.prestation._id);
    }

    getMoneyLabel = () => {
        if(this.props.prestation.paid == this.props.prestation.facturation){
            return(
                <Label basic color="green">
                    {this.props.prestation.paid} €
                    <hr/>
                    {this.props.prestation.facturation} €
                </Label>
            )
        }else{
            return(
                <Label basic color="orange">
                    {this.props.prestation.paid} €
                    <hr/>
                    {this.props.prestation.facturation} €
                </Label>
            )
        }
        
    }

    getStateOfPrestation = () => {
        const step = this.props.prestation.step;
        let stateColor,stateLabel,stateIcon;
        switch (true) {
            case (step == 0):
                stateColor = "red";
                stateLabel = "Crée";
                stateIcon = "wordpress forms";
              break;
            case (step == 1):
                stateColor = "orange";
                stateLabel = "En cours";
                stateIcon = "food";
              break;
            case (step == 2):
                stateColor = "yellow";
                stateLabel = "Payée";
                stateIcon = "credit card";
              break;
            case (step == 3):
                stateColor = "green";
                stateLabel = "Livrée";
                stateIcon = "shipping";
              break;
            case (step == 4):
                stateColor = "grey";
                stateLabel = "Archivée";
                stateIcon = "archive";
              break;
            default:
                stateColor = "black";
                stateLabel = "Erreur";
                stateIcon = "cancel";
              break;
          }
        return (
            <Button color={stateColor} animated='fade'>
                <Button.Content hidden>{stateLabel}</Button.Content>
                <Button.Content visible>
                    <Icon name={stateIcon} />
                </Button.Content>
            </Button>
        )
    }

  render() {
    const { prestation } = this.props;
    let avatar, firstname, lastname;
    if(prestation.client == null){
        avatar = "000";
        firstname = "";
        lastname = "";
    }else{
        avatar = prestation.client.avatar;
        firstname = prestation.client.firstname;
        lastname = prestation.client.lastname;
    }
    return (
        <Table.Row key={prestation._id}>
            <Table.Cell style={{padding:"4px 32px"}} >{prestation.name}</Table.Cell>
            <Table.Cell style={{display:"flex",alignItems:"center"}} textAlign='center'>
                <img style={{width:"48px",height:"48px",margin:"0 16px"}} src={"/res/avatar/"+ ('000'+avatar).slice(-3) +".png"} alt="client-avatar"/>
                {firstname+" "+lastname}
            </Table.Cell>
            <Table.Cell style={{padding:"0"}} textAlign='center'>
                {this.getMoneyLabel()}
            </Table.Cell>
            <Table.Cell style={{padding:"0"}} textAlign='center'>
                {this.getStateOfPrestation()}
            </Table.Cell>
            <Table.Cell>
                <Button onClick={this.navigate} color="blue" animated='fade'>
                    <Button.Content hidden>Détails</Button.Content>
                    <Button.Content visible>
                        <Icon name='arrow right' />
                    </Button.Content>
                </Button>
            </Table.Cell>
        </Table.Row>
    )
  }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
  export default withUserContext(PrestationRow);