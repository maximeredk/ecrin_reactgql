import React, { Component, Fragment } from 'react'
import { Table,Dropdown,Input,Button,Icon,Label } from 'semantic-ui-react';
import { IngredientsContext } from '../../contexts/IngredientsContext';
import { graphql,compose,withApollo } from 'react-apollo';
import { QUPInputs } from '../atoms/QUPInputs';
import { UserContext } from '../../contexts/UserContext';
import AllergenicsPicker from './AllergenicsPicker';
import FournisseurPicker from '../atoms/FournisseurPicker';
import gql from 'graphql-tag';
import _ from 'lodash';

class IngredientRow extends Component {

    state={
        editing:false,
        openAllergenic:false,
        _id:this.props.ingredient._id,
        name:this.props.ingredient.name,
        fournisseur:this.props.ingredient.fournisseur,
        prix:this.props.ingredient.prix,
        quantite:this.props.ingredient.quantite,
        allergenics:this.props.ingredient.allergenics,
        unite:this.props.ingredient.unite,
        allAllergenics:["almond","celeri","crusta","egg","fish","gluten","lupin","milk","mollus","mustard","peanut","sesame","soja","sulfite"].map((a,i)=>{
            return({name:a,index:i})
        }),
        units:[
            { key: 'g', text: 'g', value: 'g' },
            { key: 'Kg', text: 'Kg', value: 'Kg' },
            { key: 'mL', text: 'mL', value: 'mL' },
            { key: 'L', text: 'L', value: 'L' },
            { key: 'cm', text: 'cm', value: 'cm' },
            { key: 'm', text: 'm', value: 'm' },
            { key: 'unit', text: 'unité', value: 'unit' }
        ]
    }

    handleChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    saveChange = () => {
        this.props.editIngredient({
            _id:this.state._id,
            name:this.state.name,
            prix:parseFloat(this.state.prix),
            quantite:parseFloat(this.state.quantite),
            unite:this.state.unite,
            fournisseur:this.state.fournisseur
        })
        this.toggleEdit();
    }
    
    cancelChange = () => {
        this.toggleEdit();
    }

    toggleEdit = () => {
        this.setState({
            editing:!this.state.editing
        })
    }

    removeIngredient = id => {
        this.props.removeIngredient(id);
    }

    getAllergenics = () => {
        if(this.state.allergenics.every(a=>!a)){
            return (<Button basic circular color='black' icon='lab' onClick={this.openAllergenic} />)
        }
        return this.state.allAllergenics.map((a,i)=>{
            if(this.state.allergenics[i]){
                return (
                    <img title={this.props.getFullName(a.name)}key={"on-" + a.name} name={a.name} onClick={this.openAllergenic} style={{width:"32px",height:"32px",cursor:"pointer"}} src={"/res/allergenic/"+ a.name +".png"} alt={a.name}/>
                )
            }
        })
    }

    setAllergenics = allergenics => {
        this.props.setAllergenics({_id:this.state._id,allergenics:allergenics})
    }

    openAllergenic = () => {
        this.setState({
            openAllergenic:true
        })
    }

    closeAllergenic = () => {
        this.setState({
            openAllergenic:false
        })
    }

    changeFournisseur = fournisseur => {
        this.setState({fournisseur:fournisseur});
    }

    getFournisseurLabel = () => {
        const c = [
        {value:"",color:"grey"},
        {value:"Pro à pro",color:"blue"},
        {value:"GMD",color:"green"},
        {value:"Patisfrance",color:"red"},
        {value:"Pedrero",color:"yellow"},
        {value:"Coup de pâte",color:"orange"},
        {value:"Thecakedecorating",color:"violet"},
        {value:"Planète gâteau",color:"teal"},
        {value:"Autour du gâteau",color:"purple"}].filter(x=>x.value == this.props.ingredient.fournisseur)[0].color;
        if(this.props.ingredient.fournisseur != ""){
            return <Label color={c}>{this.props.ingredient.fournisseur}</Label>
        }else{
            return "";
        }
        
    }

  render() {
    const { _id, name, prix, quantite, unite, units, fournisseur } = this.state;
    if(!this.state.editing){
        return (
            <Fragment>
                <AllergenicsPicker setAllergenics={this.setAllergenics} id={this.state._id} open={this.state.openAllergenic} close={this.closeAllergenic} allergenics={this.state.allergenics}/>
                <Table.Row key={_id}>
                    <Table.Cell style={{padding:"0 32px",fontSize:"1.3em"}}>{name}</Table.Cell>
                    <Table.Cell textAlign='center'>
                        {this.getFournisseurLabel()}
                    </Table.Cell>
                    <Table.Cell textAlign='center'>{this.getAllergenics()}</Table.Cell>
                    <Table.Cell textAlign='center'>{prix} €</Table.Cell>
                    <Table.Cell textAlign='center'>{quantite + " " + unite}</Table.Cell>
                    <Table.Cell textAlign='center' style={{display:"flex",justifyContent:"center"}}>
                        <Dropdown style={{height:"28px",width:"117px",padding:"7px 0"}} text='Actions' icon='caret square down' floating labeled button className='icon'>
                            <Dropdown.Menu>
                                <Dropdown.Item style={{color:"#2980b9"}} icon="edit outline" text="Editer" onClick={this.toggleEdit}/>
                                <Dropdown.Item style={{color:"#e74c3c"}} icon="trash" text="Supprimer" onClick={() => {this.removeIngredient(_id)}}/>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Table.Cell>
                </Table.Row>
            </Fragment>
        )
    }else{
        return (
            <Fragment>
                <AllergenicsPicker id={this.state._id} open={this.state.openAllergenic} close={this.closeAllergenic} allergenics={this.state.allergenics}/>
                <Table.Row key={_id}>
                <Table.Cell style={{padding:"0 32px 0 48px"}}>
                        <Input placeholder='Nom' defaultValue={name} name='name' onChange={this.handleChange}/>
                    </Table.Cell>
                    <Table.Cell textAlign='center'>
                        <FournisseurPicker changeFournisseur={this.changeFournisseur} fournisseur={fournisseur}/>
                    </Table.Cell>
                    <Table.Cell textAlign='center'>{this.getAllergenics()}</Table.Cell>
                    <QUPInputs units={units} prix={prix} quantite={quantite} unite={unite} handleChange={this.handleChange} />
                    <Table.Cell style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
                        <Button onClick={this.saveChange} color="blue" animated='fade'>
                            <Button.Content hidden>Valider</Button.Content>
                            <Button.Content visible>
                                <Icon name='check' />
                            </Button.Content>
                        </Button>
                        <Button onClick={this.cancelChange} color="red" animated='fade'>
                            <Button.Content hidden>Annuler</Button.Content>
                            <Button.Content visible>
                                <Icon name='cancel' />
                            </Button.Content>
                        </Button>
                    </Table.Cell>
                </Table.Row>
            </Fragment>
        )
    }
  }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  

const withIngredientsContext = WrappedComponent => props => (
    <IngredientsContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </IngredientsContext.Consumer>
  )
  
  export default wrappedIngredientsContext = withApollo(withUserContext(withIngredientsContext(IngredientRow)));