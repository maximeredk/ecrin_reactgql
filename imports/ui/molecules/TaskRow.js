import React, { Component } from 'react';
import { Label,Icon } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';

class TaskRow extends Component {

    state={
      task : this.props.task,
      gridStart : (this.props.index % 2 + 2),
      completeTaskQuery : gql`
        mutation completeTask($_id: String!){
          completeTask(_id: $_id) {
            _id
            content
            completed
          }
        }
      `
    }

    completeTask = () => {
      this.props.client.mutate({
        mutation:this.state.completeTaskQuery,
        variables:{
          _id:this.state.task._id
        }
      }).then((data)=>{
        this.setState({
          task:data.data.completeTask
        })
      })
    }

    deleteTask = () => {
      this.props.deleteTask(this.state.task._id);
    }

  render() {
    const { task,gridStart } = this.state;
    if(task.completed){
      return (
        <div style={{fontFamily:"'Patrick Hand', cursive",fontSize:"2em",gridColumnStart:gridStart,width:"100%",padding:"16px 20px",backgroundColor:"rgba(39,174,96,0.3)",borderRadius:"4px"}}>
          {task.content}
            <Label onClick={this.deleteTask} circular style={{float:"right",margin:"4px",cursor:"pointer"}} size="big" color="black">
              <Icon style={{margin:"0"}} name="trash"/>
            </Label>
            <Label onClick={this.completeTask} circular style={{float:"right",margin:"4px",cursor:"pointer"}} size="big" color="black">
              <Icon color="orange" style={{margin:"0"}} name="cancel"/>
            </Label>
        </div>
      )
    }else{
      return (
        <div style={{fontFamily:"'Patrick Hand', cursive",fontSize:"2em",gridColumnStart:gridStart,width:"100%",padding:"16px 20px",backgroundColor:"rgba(18,137,167,0.3)",borderRadius:"4px"}}>
          {task.content}
            <Label onClick={this.deleteTask} circular style={{float:"right",margin:"4px",cursor:"pointer"}} size="big" color="black">
              <Icon style={{margin:"0"}} name="trash"/>
            </Label>
            <Label onClick={this.completeTask} circular style={{float:"right",margin:"4px",cursor:"pointer"}} size="big" color="black">
              <Icon color="green" style={{margin:"0"}} name="check"/>
            </Label>
        </div>
      )
    }
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(TaskRow);