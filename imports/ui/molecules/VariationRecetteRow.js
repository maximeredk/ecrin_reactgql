import React, { Component } from 'react'
import { Table,Dropdown,Button,Icon } from 'semantic-ui-react';
import { withApollo } from 'react-apollo';
import { QUInputs } from '../atoms/QUInputs';
import _ from 'lodash';

class VariationIngredientRow extends Component {

    state={
        _id:this.props.vrecette._id,
        name:this.props.vrecette.ingredient.name,
        prix:this.props.vrecette.ingredient.prix,
        quantite:this.props.vrecette.ingredient.quantite,
        unite:this.props.vrecette.ingredient.unite
    }

    deleteVRecette = () => {
        this.props.deleteVRecette(this.state._id);
    }

    formatCS = n =>{
        return parseFloat(Math.round(n * 100) / 100).toFixed(2);
    }

  render() {
    const { _id, name, prix, quantite, unite } = this.state;
    return (
        <Table.Row key={_id}>
            <Table.Cell style={{padding:"0 32px",fontSize:"1.3em"}}>
                {name}
            </Table.Cell>
            <Table.Cell textAlign='center'>{this.formatCS(prix)} €</Table.Cell>
            <Table.Cell textAlign='center'>{quantite + " " + unite}</Table.Cell>
            <Table.Cell style={{padding:"4px 0",display:"flex",justifyContent:"center",alignItems:"center"}}>
                <Dropdown style={{height:"28px",width:"117px",padding:"7px 0"}} text='Actions' icon='caret square down' floating labeled button className='icon'>
                    <Dropdown.Menu>
                        <Dropdown.Item style={{color:"#e74c3c"}} icon="trash" text="Supprimer" onClick={this.deleteVRecette}/>
                    </Dropdown.Menu>
                </Dropdown>
            </Table.Cell>
        </Table.Row>
    )
  }
}

export default withApollo(VariationIngredientRow);