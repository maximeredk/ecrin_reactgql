import React, { Component } from 'react';
import { Table,Dropdown } from 'semantic-ui-react';
import { RecettesContext } from '../../contexts/RecettesContext';
import { UserContext } from '../../contexts/UserContext'
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

class RecetteRow extends Component {

    navigate = () => {
        this.props.navigateToRecette(this.props.recette._id);
    }

    removeRecette = id => {
        this.props.removeRecette({
            variables:{
                _id:id
            }
        });
    }

  render() {
    const { recette } = this.props;
    return (
        <Table.Row key={recette._id}>
            <Table.Cell onClick={this.navigate} style={{cursor:"pointer",padding:"0 32px 0 48px",fontSize:"1.3em"}}>{recette.name}</Table.Cell>
            <Table.Cell style={{padding:"8px 0",display:"flex",justifyContent:"center",alignItems:"center"}}>
                <Dropdown style={{height:"28px",width:"117px",padding:"7px 0"}} text='Actions' icon='caret square down' floating labeled button className='icon'>
                    <Dropdown.Menu>
                        <Dropdown.Item style={{color:"#2980b9"}} icon="caret square right" text="Détails" onClick={this.navigate}/>
                        <Dropdown.Item style={{color:"#e74c3c"}} icon="trash" text="Supprimer" onClick={() => {this.removeRecette(recette._id)}}/>
                    </Dropdown.Menu>
                </Dropdown>
            </Table.Cell>
        </Table.Row>
    )
  }
}

const withRecettesContext = WrappedComponent => props => (
    <RecettesContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </RecettesContext.Consumer>
  )

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )

const wrappedRecettesContext = withRecettesContext(RecetteRow);

const wrappedUserContext = withUserContext(wrappedRecettesContext);

const removeRecette = gql`
    mutation removeRecette($_id: String!){
    removeRecette(_id: $_id)
    }
`;

export default graphql(removeRecette,{
    name:"removeRecette",
    options:{
        refetchQueries: () => [ "Recettes" ]
    }
})(wrappedUserContext)