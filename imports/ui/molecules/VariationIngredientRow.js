import React, { Component } from 'react'
import { Table,Dropdown,Input,Button,Icon } from 'semantic-ui-react';
import { IngredientsContext } from '../../contexts/IngredientsContext';
import { graphql,compose,withApollo } from 'react-apollo';
import { QUInputs } from '../atoms/QUInputs';
import gql from 'graphql-tag';
import _ from 'lodash';

class VariationIngredientRow extends Component {

    state={
        editing:false,
        _id:this.props.vingredient._id,
        usedQ:this.props.vingredient.usedQ,
        usedU:this.props.vingredient.usedU,
        ing_id:this.props.vingredient.ingredient._id,
        name:this.props.vingredient.ingredient.name,
        prix:this.props.vingredient.ingredient.prix,
        quantite:this.props.vingredient.ingredient.quantite,
        unite:this.props.vingredient.ingredient.unite,
        newUsedQ:this.props.vingredient.usedQ,
        newUsedU:this.props.vingredient.usedU,
        allAllergenics:["almond","celeri","crusta","egg","fish","gluten","lupin","milk","mollus","mustard","peanut","sesame","soja","sulfite"].map((a,i)=>{
            return({name:a,index:i})
        })
    }

    handleChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    saveChange = () => {
        this.props.editVIngredient(this.state._id,this.state.newUsedQ,this.state.newUsedU,this.state.prix,this.state.quantite,this.state.unite);
        this.toggleEdit();
    }
    
    getAllergenics = () => {
        return this.state.allAllergenics.map((a,i)=>{
            if(this.props.vingredient.ingredient.allergenics[i]){
                return (
                    <img key={"on-" + a.name} name={a.name} onClick={this.openAllergenic} style={{width:"32px",height:"32px",cursor:"pointer"}} src={"/res/allergenic/"+ a.name +".png"} alt={a.name}/>
                )
            }
        })
    }

    cancelChange = () => {
        this.toggleEdit();
    }

    toggleEdit = () => {
        this.setState({
            editing:!this.state.editing
        })
    }

    deleteVIngredient = () => {
        this.props.deleteVIngredient(this.state._id);
    }

    filterAvailableUnits = unite => {
        switch (true) {
            case (unite == "g"):
                return [
                    { key: 'g', text: 'g', value: 'g' },
                    { key: 'Kg', text: 'Kg', value: 'Kg' }
                ]
                break;
            case (unite == "Kg"):
                return [
                    { key: 'g', text: 'g', value: 'g' },
                    { key: 'Kg', text: 'Kg', value: 'Kg' }
                ]
                break;
            case (unite == "mL"):
                return [
                    { key: 'mL', text: 'mL', value: 'mL' },
                    { key: 'L', text: 'L', value: 'L' }
                ]
                break;
            case (unite == "L"):
                return [
                    { key: 'mL', text: 'mL', value: 'mL' },
                    { key: 'L', text: 'L', value: 'L' }
                ]
                break;
            case (unite == "cm"):
                return [
                    { key: 'cm', text: 'cm', value: 'cm' },
                    { key: 'm', text: 'm', value: 'm' }
                ]
                break;
            case (unite == "m"):
                return [
                    { key: 'cm', text: 'cm', value: 'cm' },
                    { key: 'm', text: 'm', value: 'm' }
                ]
                break;
            case (unite == "unit"):
                return [
                    { key: 'unit', text: 'unité', value: 'unit' }
                ]
                break;
            default:
                return [{ key: 'err', text: 'err', value: 'err' }]
                break;
        }
    }

    getPrix = (prix,unit) => {
        switch (true) {
            case (unit == "g"):
                return parseFloat(prix*1000).toString() + " €/Kg"
                break;
            case (unit == "Kg"):
                return prix.toString() + " €/Kg"
                break;
            case (unit == "mL"):
                return parseFloat(prix*1000).toString() + " €/L"
                break;
            case (unit == "L"):
                return prix.toString() + " €/L"
                break;
            case (unit == "cm"):
                return parseFloat(prix*100).toString() + " €/m"
                break;
            case (unit == "m"):
                return prix.toString() + " €/m"
                break;
            case (unit == "unit"):
                return prix.toString() + " €/unité"
                break;
            default:
                break;
        }
    }

    formatCS = n =>{
        return parseFloat(Math.round(n * 100) / 100).toFixed(2);
    }

  render() {
    const { _id, name, prix, quantite, unite } = this.state;
    const usedQ = this.props.vingredient.usedQ;
    const usedU = this.props.vingredient.usedU;
    const usedP = this.props.vingredient.usedP;
    if(!this.state.editing){
        return (
            <Table.Row key={_id}>
                <Table.Cell style={{padding:"0 32px",fontSize:"1.3em"}}>
                    {name}
                </Table.Cell>
                <Table.Cell textAlign='center'>{this.getAllergenics()}</Table.Cell>
                <Table.Cell textAlign='center'>{this.formatCS(usedP)} €</Table.Cell>
                <Table.Cell textAlign='center'>{usedQ + " " + usedU}</Table.Cell>
                <Table.Cell style={{padding:"4px 0",display:"flex",justifyContent:"center",alignItems:"center"}}>
                    <Dropdown style={{height:"28px",width:"117px",padding:"7px 0"}} text='Actions' icon='caret square down' floating labeled button className='icon'>
                        <Dropdown.Menu>
                            <Dropdown.Item style={{color:"#2980b9"}} icon="edit outline" text="Editer" onClick={this.toggleEdit}/>
                            <Dropdown.Item style={{color:"#e74c3c"}} icon="trash" text="Supprimer" onClick={this.deleteVIngredient}/>
                        </Dropdown.Menu>
                    </Dropdown>
                </Table.Cell>
            </Table.Row>
        )
    }else{
        return (
            <Table.Row key={_id}>
                <Table.Cell style={{padding:"0 32px",fontSize:"1.3em"}}>
                    {name}
                </Table.Cell>
                <Table.Cell textAlign='center'>{this.getAllergenics()}</Table.Cell>
                <Table.Cell textAlign='center'>{this.formatCS(usedP)} €</Table.Cell>
                <QUInputs units={this.filterAvailableUnits(unite)} quantite={usedQ} unite={usedU} handleChange={this.handleChange} />
                <Table.Cell style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
                    <Button onClick={this.saveChange} color="blue" animated='fade'>
                        <Button.Content hidden>Valider</Button.Content>
                        <Button.Content visible>
                            <Icon name='check' />
                        </Button.Content>
                    </Button>
                    <Button onClick={this.cancelChange} color="red" animated='fade'>
                        <Button.Content hidden>Annuler</Button.Content>
                        <Button.Content visible>
                            <Icon name='cancel' />
                        </Button.Content>
                    </Button>
                </Table.Cell>
            </Table.Row>
        )
    }
  }
}

export default withApollo(VariationIngredientRow);