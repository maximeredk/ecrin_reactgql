import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react';
import { ClientsContext } from '../../contexts/ClientsContext';

class ClientTile extends Component {
  
  state={
    client:this.props.client
  }

  navigate = () => {
    this.props.navigateToClient(this.state.client._id);
  }

  render() {
    const { client } = this.state;
    return (
      <div onClick={this.navigate} style={{cursor:"pointer",width:"164px",height:"134px",margin:"32px 8px",display:"grid",gridTemplateColumns:"40px 84px 40px",gridTemplateRows:"84px 1fr"}}>
          <img style={{width:"84px",height:"84px",gridColumnStart:"2"}} src={"/res/avatar/"+ ('000'+client.avatar).slice(-3) +".png"} alt="client-avatar"/>
          <br/>
          <Segment textAlign='center' style={{gridColumnStart:"1",gridRowStart:"2",gridColumnEnd:"span 3",fontSize:"1.2em"}}>
            {client.lastname+" "+client.firstname}
          </Segment>
      </div>
    )
  }
}

const withClientContext = WrappedComponent => props => (
  <ClientsContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </ClientsContext.Consumer>
)

export default withClientContext(ClientTile);