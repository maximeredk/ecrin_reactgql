import React, { Component } from 'react'
import { Divider,Image,Modal,Header,Icon } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import _ from 'lodash';

class AllergenicsPicker extends Component {

    state = {
        allAllergenics:["almond","celeri","crusta","egg","fish","gluten","lupin","milk","mollus","mustard","peanut","sesame","soja","sulfite"].map((a,i)=>{
            return({name:a,index:i})
        })
    }

    handleOff = e => {
        let a = this.props.allergenics
        a[e.target.name] = true;
        this.props.setAllergenics(a);
    }

    handleOn = e => {
        let a = this.props.allergenics
        a[e.target.name] = false;
        this.props.setAllergenics(a);
    }

    getOnAllergenics = () => {
        return this.state.allAllergenics.map((a,i)=>{
            if(this.props.allergenics[i]){
                return (
                    <Image title={this.props.getFullName(a.name)} key={"on-" + a.name} name={i} onClick={this.handleOn} width="52px" height="52px" style={{margin:"4px",cursor:"pointer"}} src={"/res/allergenic/"+ a.name +".png"} alt={a.name}/>
                )
            }
        })
    }

    getOffAllergenics = () => {
        return this.state.allAllergenics.map((a,i)=>{
            if(!this.props.allergenics[i]){
                return (
                    <Image title={this.props.getFullName(a.name)} key={"off-" + a.name} name={i} onClick={this.handleOff} width="52px" height="52px" style={{filter:"grayscale(100%)",cursor:"pointer"}} src={"/res/allergenic/"+ a.name +".png"} alt={a.name}/>
                )
            }
        })
    }

    render() {
        return (
            <Modal dimmer="inverted" size="small" open={this.props.open} onClose={this.props.close} closeIcon>
                <Divider horizontal>
                    <Header as='h4'>
                        <Icon color="green" name='check' style={{margin:"0"}} />
                    </Header>
                </Divider>
                <div style={{display:'flex',flexWrap:"wrap",justifyContent:"space-evenly"}}>
                    {this.getOnAllergenics()}
                </div>
                <Divider horizontal>
                    <Header as='h4'>
                        <Icon name='arrows alternate vertical' style={{margin:"0"}} />
                    </Header>
                </Divider>
                <div style={{display:'flex',flexWrap:"wrap",justifyContent:"space-evenly"}}>
                    {this.getOffAllergenics()}
                </div>
                <Divider horizontal>
                    <Header as='h4'>
                        <Icon color="red" name='cancel' style={{margin:"0"}} />
                    </Header>
                </Divider>
            </Modal>
        )
    }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
  export default withUserContext(AllergenicsPicker);