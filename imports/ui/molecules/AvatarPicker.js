import React, { Component, Fragment } from 'react';
import { Modal } from 'semantic-ui-react';

export class AvatarPicker extends Component {

    state={
        open:false,
        avatar:this.props.avatar
    }

    close = () => {
        this.setState({
            open:false
        })
    }
    
    toggleAvatarPicker = () => {
        this.setState({
            open:true
        })
    }

    chooseAvatar = id => {
        this.props.editAvatar(id);
        this.setState({
            avatar: id,
            open:false
        })
    }

  render() {
    let avatars = [];
    for(let i = 1; i <= 260; i++) {
        avatars.push(i);
    }
    return (
        <Fragment>
            <Modal size="large" closeIcon open={this.state.open} onClose={this.close} trigger={
                <div style={{gridRowStart:"2",gridColumnStart:"1",gridColumnEnd:"span 3"}}>
                    <img onClick={this.toggleAvatarPicker} style={{cursor:"pointer",width:"160px",height:"160px",margin:"32px 0 32px 0"}} src={"/res/avatar/"+ ('000'+this.props.avatar).slice(-3) +".png"} alt="active-client-avatar"/>
                </div>
            }>
                <Modal.Header>
                    Choisir un avatar :
                </Modal.Header>
                <Modal.Content scrolling>
                    <Modal.Description style={{display:"flex",flexWrap:"wrap",justifyContent:"space-around"}}>
                        {avatars.map(avatar=>(
                            <img onClick={() => {this.chooseAvatar(avatar)}} key={avatar} style={{cursor:"pointer",width:"80px",height:"80px",margin:"16px"}} src={"/res/avatar/"+ ('000'+avatar).slice(-3) +".png"} alt="active-client-avatar"/>
                        ))}
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        </Fragment>
    )
  }
}

export default AvatarPicker
