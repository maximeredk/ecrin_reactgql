import React, { Component, Fragment } from 'react'
import VariationIngredientRow from '../molecules/VariationIngredientRow';
import { Dropdown, Table, Segment, Modal, Statistic, Button } from 'semantic-ui-react';
import { withApollo } from 'react-apollo';
import _ from 'lodash';
import gql from 'graphql-tag';

class RecetteVariation extends Component {

    state={
        variation:{_id:null,vingredients:[],vrecettes:[]},
        variationQuery : gql`query variation($_id: String!){
            variation(_id: $_id) {
                _id
                name
                vingredients{
                    _id
                    usedQ
                    usedU
                    usedP
                    ingredient{
                        _id
                        name
                        prix
                        quantite
                        unite
                        allergenics
                    }
                }
                vrecettes{
                    _id
                    ingredient{
                        _id
                        name
                        prix
                        quantite
                        unite
                        allergenics
                    }
                }
            }
        }`,
        ingredientsQuery : gql`query ingredients{
            ingredients{
                _id
                name
                prix
                quantite
                unite
                allergenics
            }
        }`,
        ingredientsFromVariationQuery : gql`query ingredients{
            ingredientsFromVariation{
                _id
                name
                prix
                quantite
                unite
                allergenics
            }
        }`,
        ingredients:[],
        ingredientsFromVariation:[],
        addVIngredientToVariation : gql`mutation addVIngredientToVariation($variation: String!,$ingredient: String!){
            addVIngredientToVariation(variation: $variation, ingredient: $ingredient)
        }`,
        deleteVIngredient : gql`mutation deleteVIngredient($_id: String!){
            deleteVIngredient(_id:$_id)
        }`,
        editVIngredient : gql`mutation editVIngredient($_id:String!,$usedQ:Float!,$usedU:String!,$usedP:Float!){
            editVIngredient(_id:$_id, usedQ:$usedQ, usedU:$usedU, usedP:$usedP)
        }`,
        allergenicsNames:["almond","celeri","crusta","egg","fish","gluten","lupin","milk","mollus","mustard","peanut","sesame","soja","sulfite"],
        allergenics:["almond","celeri","crusta","egg","fish","gluten","lupin","milk","mollus","mustard","peanut","sesame","soja","sulfite"].map((a,i)=>{
            return({name:a,isIn:false,index:i})
        }),
        openTag:false
    }

    getVariation = () => {
        this.props.client.query({
            query:this.state.variationQuery,
            variables:{ _id:this.props.activeVariationId },
            fetchPolicy:"network-only"
        }).then(({data})=>{
            let alls = [];
            if(data.variation.vingredients.length > 0){
                alls = data.variation.vingredients.map(a => a.ingredient.allergenics).reduce((a, b) => a.map((c, i) => b[i] || c)).map((al,i) => {return({index:i,name:this.state.allergenicsNames[i],isIn:al})})
            }
            this.setState({
                variation:data.variation,
                allergenics:alls
            }) 
        })
    }

    removeVariation = () => {
        this.props.removeVariation(this.props.activeVariationId);
    }

    getIngredients = () => {
        this.props.client.query({
            query:this.state.ingredientsQuery,
            fetchPolicy:"network-only"
        }).then(({data})=>{
            this.setState({
                ingredients : data.ingredients.map(i=>{
                    return {key:i._id,value:i._id,text:i.name}
                })
            });
        })
    }

    getIngredientsFromVariation = () => {
        this.props.client.query({
            query:this.state.ingredientsFromVariationQuery,
            fetchPolicy:"network-only"
        }).then(({data})=>{
            this.setState({
                ingredientsFromVariation : data.ingredientsFromVariation.map(i=>{
                    return {key:i._id,value:i._id,text:i.name}
                })
            });
        })
    }

    addVIngredientToVariation = (e, { value }) => {
        this.props.client.mutate({
            mutation:this.state.addVIngredientToVariation,
            variables:{ variation:this.state.variation._id,ingredient:value}
        }).then(()=>{
            this.props.client.query({
                query:this.state.variationQuery,
                variables:{ _id:this.state.variation._id },
                fetchPolicy:"network-only"
            }).then(({data})=>{
                this.setState({
                    variation:data.variation
                }) 
            })
        })
    }
    
    deleteVIngredient = vingredient => {
        this.props.client.mutate({
            mutation:this.state.deleteVIngredient,
            variables:{ _id:vingredient}
        }).then(()=>{
            this.props.client.query({
                query:this.state.variationQuery,
                variables:{ _id:this.state.variation._id },
                fetchPolicy:"network-only"
            }).then(({data})=>{
                this.setState({
                    variation:data.variation
                }) 
            })
        })
    }

    deleteVRecette = vrecette => {
        this.props.client.mutate({
            mutation:this.state.deleteVRecette,
            variables:{ _id:vrecette}
        }).then(()=>{
            this.props.client.query({
                query:this.state.variationQuery,
                variables:{ _id:this.state.variation._id },
                fetchPolicy:"network-only"
            }).then(({data})=>{
                this.setState({
                    variation:data.variation
                }) 
            })
        })
    }

    editVIngredient = (vingredient,usedQ,usedU,prix,quantite,unite) => {
        this.props.client.mutate({
            mutation:this.state.editVIngredient,
            variables:{
                _id:vingredient,
                usedQ:parseFloat(usedQ),
                usedU:usedU,
                usedP:this.convertPrix(quantite,unite,prix,usedQ,usedU)
            }
        }).then(()=>{
            this.props.client.query({
                query:this.state.variationQuery,
                variables:{ _id:this.state.variation._id },
                fetchPolicy:"network-only"
            }).then(({data})=>{
                this.setState({
                    variation:data.variation
                }) 
            })
        })
    }

    convertPrix = (ingrQfrom, ingrUfrom, ingrP, ingrQto, ingrUto) => {
        if(ingrP == 0 || ingrQfrom == 0){
            return parseFloat(0);
        }
        let fac = 0;
        if(ingrUfrom=="g" && ingrUto=="mL"){fac = 1;}
        if(ingrUfrom=="mL" && ingrUto=="g"){fac = 1;}
        if(ingrUfrom=="Kg" && ingrUto=="L"){fac = 1;}
        if(ingrUfrom=="L" && ingrUto=="Kg"){fac = 1;}
        if(ingrUfrom=="mL" && ingrUto=="Kg"){fac = 1000;}
        if(ingrUfrom=="Kg" && ingrUto=="mL"){fac = 0.001;}
        if(ingrUfrom=="L" && ingrUto=="g"){fac = 0.001;}
        if(ingrUfrom=="g" && ingrUto=="L"){fac = 1000;}
        if(ingrUfrom=="g" && ingrUto=="g"){fac = 1;}
        if(ingrUfrom=="g" && ingrUto=="Kg"){fac = 1000;}
        if(ingrUfrom=="Kg" && ingrUto=="g"){fac = 0.001;}
        if(ingrUfrom=="Kg" && ingrUto=="Kg"){fac = 1;}
        if(ingrUfrom=="mL" && ingrUto=="L"){fac = 1000;}
        if(ingrUfrom=="mL" && ingrUto=="mL"){fac = 1;}
        if(ingrUfrom=="L" && ingrUto=="L"){fac = 1;}
        if(ingrUfrom=="L" && ingrUto=="mL"){fac = 0.001;}
        if(ingrUfrom=="cm" && ingrUto=="cm"){fac = 1;}
        if(ingrUfrom=="cm" && ingrUto=="m"){fac = 100;}
        if(ingrUfrom=="m" && ingrUto=="m"){fac = 1;}
        if(ingrUfrom=="m" && ingrUto=="cm"){fac = 0.01;}
        if(ingrUfrom=="unit" && ingrUto=="unit"){fac = 1;}
        if(fac==0){
            return "error, no match bewteen these ingr units";
        }
        let res = (ingrQto/(ingrQfrom/fac))*ingrP;
        return res;
    }

    componentDidMount = () => {
        this.getVariation();
        this.getIngredients();
        //this.getIngredientsFromVariation();
    }

    getAllergenics = () => {
        if(this.state.allergenics.every(a=>a.isIn==false)){
            return <p> Aucun allergène dans cette recette </p>
        }
        let all = [false,false,false,false,false,false,false,false,false,false,false,false,false,false];
        this.state.variation.vingredients.map(vi=>{
            vi.ingredient.allergenics.map((a,i)=>{
                if(a){
                    all[i] = true;
                }
            })
        })
        return (this.state.allergenics.map((a,i)=>{
            if(all[i]){  
                return (
                    <img key={"on-" + a.name} name={a.name} onClick={this.openAllergenic} style={{width:"32px",height:"32px",cursor:"pointer"}} src={"/res/allergenic/"+ a.name +".png"} alt={a.name}/>
                )
            }
        }))
    }

    formatCS = n =>{
        return parseFloat(Math.round(n * 100) / 100).toFixed(2);
    }

    getPrix = () => {
        const res = this.formatCS(_.sumBy(this.state.variation.vingredients,"usedP")+_.sumBy(this.state.variation.vrecettes,"ingredient.prix")).toString().split(".");
        return (
            <Fragment>
                {res[0]} 
                <span style={{fontSize:"0.5em"}}>
                    {","+res[1]}
                </span>
            </Fragment>
        )
    }

    getFacture = () => {
        const res = this.formatCS((_.sumBy(this.state.variation.vingredients,"usedP")+_.sumBy(this.state.variation.vrecettes,"ingredient.prix")) * 3).toString().split(".");
        return (
            <Fragment>
                {res[0]} 
                <span style={{fontSize:"0.5em"}}>
                    {","+res[1]}
                </span>
            </Fragment>
        )
    }

    openTag = () => {
        this.setState({
            openTag:true
        })
    }

    closeTag = () => {
        this.setState({
            openTag:false
        })
    }

    render() {
        const { variation, ingredients } = this.state;
        if(variation._id != this.props.activeVariationId){
            this.getVariation()
        }else{
            if(variation != null){
                return (
                    <Fragment>
                        <div style={{display:"grid",gridGap:"32px",height:"100%",width:"100%",gridTemplateColumns:"1fr 3fr",gridTemplateRows:"auto"}}>
                            <Segment style={{margin:"0",placeSelf:"stretch"}}>
                                <div style={{display:"flex",justifyContent:"center"}}>
                                    {this.getAllergenics()}
                                </div>
                            </Segment>
                            <Segment textAlign="center" style={{heigth:"256px",margin:"0",gridRowStart:"2",placeSelf:"stretch"}}>
                                <Statistic style={{margin:"0"}} color="green">
                                    <Statistic.Label style={{textAlign:"center",letterSpacing:"0.2em"}}>
                                        Coût de revient
                                    </Statistic.Label>
                                    <Statistic.Value style={{lineHeight:"64px",fontFamily: "'Merienda', sans-serif",fontSize:"3.5em",textAlign:"center"}}>
                                        {this.getPrix()} €
                                    </Statistic.Value>
                                </Statistic>
                                <Statistic style={{margin:"0"}} color="blue">
                                    <Statistic.Label style={{textAlign:"center",letterSpacing:"0.2em"}}>
                                        Montant à facturer
                                    </Statistic.Label>
                                    <Statistic.Value style={{lineHeight:"64px",fontFamily: "'Merienda', sans-serif",fontSize:"3.5em",textAlign:"center"}}>
                                        {this.getFacture()} €
                                    </Statistic.Value>
                                </Statistic>
                                <Button basic color='purple' content="Impression de l'étiquette" onClick={this.openTag}/>
                            </Segment>
                            <Table striped style={{margin:"0",gridRowStart:"1",gridColumnStart:"2",gridRowEnd:"span 2",justifySelf:"stretch",alignSelf:"start"}}>
                                <Table.Header>
                                    <Table.Row textAlign='center'>
                                    <Table.HeaderCell width={6}>Ingredient</Table.HeaderCell>
                                    <Table.HeaderCell width={2}>Allergènes</Table.HeaderCell>
                                    <Table.HeaderCell width={2}>Prix</Table.HeaderCell>
                                    <Table.HeaderCell width={2}>Quantité</Table.HeaderCell>
                                    <Table.HeaderCell width={4}>
                                        <Dropdown placeholder='Ajouter un ingredient' selectOnNavigation={false} fluid search selection options={ingredients} onChange={this.addVIngredientToVariation}/>
                                    </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {variation.vingredients.map(vi=> {
                                        return(
                                            <VariationIngredientRow key={vi._id} editVIngredient={this.editVIngredient} deleteVIngredient={this.deleteVIngredient} vingredient={vi}/>
                                        )
                                    })}
                                </Table.Body>
                            </Table>
                        </div>
                        <Modal dimmer="inverted" size={"mini"} open={this.state.openTag} onClose={this.closeTag}>
                            <Modal.Content>
                                <Statistic style={{margin:"auto",width:"100%",fontSize:"1.5em"}}>
                                    <Statistic.Label style={{textAlign:"center",letterSpacing:"0.2em"}}>
                                        {this.props.recipeName}
                                    </Statistic.Label>
                                    <Statistic.Value style={{margin:"8px auto",lineHeight:"64px",fontFamily: "'Merienda', sans-serif",fontSize:"3.7em",textAlign:"center",backgroundColor:"#F67062",backgroundImage:"linear-gradient(326deg, #F67062 0%, #FC5296 74%)",backgroundClip: "text",WebkitBackgroundClip: "text",color: "transparent"}}>
                                        {this.getFacture()} €
                                    </Statistic.Value>
                                </Statistic>
                                <hr style={{width:"90%"}}/>
                                <div style={{marginTop:"16px",display:"flex",justifyContent:"center"}}>
                                    {this.getAllergenics()}
                                </div>
                            </Modal.Content>
                        </Modal>
                    </Fragment>
                )
            }else{
                return("Loading");
            }
        }
        return("Loading");
    }
}

export default withApollo(RecetteVariation);

