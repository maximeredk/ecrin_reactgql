import React, { Component } from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
import PageBody from './pages/PageBody';
import Menu from './menu/Menu';
import Home from './pages/Home';
import NeedActivation from './pages/NeedActivation';
import { Button, Icon } from 'semantic-ui-react';
import { UserContext } from '../contexts/UserContext';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class AppBody extends Component{

    componentDidMount = () => {
        toast.configure()
    }

    logout = () => {
        Meteor.logout();
        this.props.client.resetStore();
    }
    
    render(){
        if(this.props.user._id != null){
            if(this.props.user.activated){
                return(
                    <div style={{width:"100vw",minWidth:"780px",minHeight:"100vh"}}>
                        <Menu/>
                        <ToastContainer position="top-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl pauseOnVisibilityChange draggable pauseOnHover/>
                        <PageBody />
                    </div>
                );
            }else{
                return(
                    <div style={{
                        width:"100vw",
                        height:"100vh",
                        margin:"0",
                        padding:"32px 128px 0 160px",
                        display:"inline-block",
                        linearGradient:"(315deg, #fffsff 0%, #d1e12c 74%)",
                        backgroundImage:"url('/res/wall/backg"+6+".png')",
                        backgroundRepeat:"no-repeat",
                        backgroundAttachment:"fixed"
                    }}>
                        <div style={{display:"grid",marginTop:"80px",gridTemplateColumns:"1fr 250px 480px 250px 1fr",gridTemplateRows:"480px 80px 80px",flexWrap:"wrap",justifyContent:"center",width:"100%"}}>
                            <img style={{width:"400px",gridColumnStart:"2",gridColumnEnd:"span 3",placeSelf:"center"}} src={"/res/title.png"} alt="titleLogo"/>
                            <NeedActivation/>
                            <Button size="small" style={{marginTop:"24px",width:"128px",gridColumnStart:"2",gridColumnEnd:"span 3",placeSelf:"center",cursor:"pointer"}} onClick={this.logout} basic color='red' animated="fade">
                                <Button.Content visible>Déconnexion</Button.Content>
                                <Button.Content hidden><Icon name='arrow left'/></Button.Content>
                            </Button>
                        </div>
                    </div>
                );
            }
        }else{
            return(
                <div style={{
                    width:"100vw",
                    height:"100vh",
                    margin:"0",
                    padding:"32px 128px 0 160px",
                    display:"inline-block",
                    linearGradient:"(315deg, #fffsff 0%, #d1e12c 74%)",
                    backgroundImage:"url('/res/wall/backg"+6+".png')",
                    backgroundRepeat:"no-repeat",
                    backgroundAttachment:"fixed"
                }}>
                  <ToastContainer position="bottom-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl pauseOnVisibilityChange draggable pauseOnHover/>
                  <Switch>
                    <Route path='/' component={Home}/>
                    <Redirect from='*' to={'/'}/>
                  </Switch>
                </div>
            )
        }
    }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
  export default withUserContext(AppBody);