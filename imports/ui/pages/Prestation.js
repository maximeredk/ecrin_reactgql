import React, { Component } from 'react';
import { PrestationsContext } from '../../contexts/PrestationsContext';
import { Button, Icon, Step, Statistic, Segment, Header, TextArea, Input } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import { graphql,compose,withApollo } from 'react-apollo';
import _ from 'lodash';
import gql from 'graphql-tag';


class Prestation extends Component {

    state ={
      prestation:null,
      setStepQuery:gql`mutation setStep($_id: String!,$step: Int!){
        setStep(_id: $_id, step: $step){
            _id
            step
        }
      }`,
      setPaidQuery:gql`mutation setPaid($_id: String!,$paid: Float!){
        setPaid(_id: $_id, paid: $paid){
            _id
            paid
        }
      }`,
      setFacturationQuery:gql`mutation setFacturation($_id: String!,$facturation: Float!){
        setFacturation(_id: $_id, facturation: $facturation){
            _id
            facturation
        }
      }`,
      setDescriptionQuery:gql`mutation setDescription($_id: String!,$description: String!){
        setDescription(_id: $_id, description: $description){
            _id
            description
        }
      }`,
      step:0,
      editingPaid:false,
      editingFacturation:false,
      newPaid:0,
      newFacturation:0,
      newDescription:""
    }

    handleChange = e =>{
      this.setState({
        [e.target.name]:e.target.value
      });
    }

    getPrestationById = _id => {
      this.props.client.query({
          query : gql`
              query Prestation($_id: String!) {
                prestation(_id: $_id) {
                    _id
                    name
                    client{
                        _id
                        firstname
                        lastname
                        avatar
                        phone
                        mail
                        address
                        zipcode
                        city
                    }
                    deliveryDay
                    deliveryMonth
                    deliveryYear
                    description
                    facturation
                    paid
                    archived
                    step
                }
              }
          `,
          variables:{_id:_id},
          fetchPolicy:"network-only"
      }).then(({data})=>{
        this.setState({
            prestation:data.prestation,
            step:data.prestation.step,
            paid:data.prestation.paid,
            description:data.prestation.description,
            facturation:data.prestation.facturation,
            newPaid:data.prestation.paid,
            newFacturation:data.prestation.facturation,
            newDescription:data.prestation.description
        });
      })
    }

    removePrestation = _id => {
      this.props.removePrestation({
          variables:{
              _id:_id
          }
      });
      this.props.history.push("/prestations/"+new Date().getFullYear()+"/"+parseInt(new Date().getMonth()+1));
    }

    componentDidMount = () => {
      this.getPrestationById(this.props.match.params.id);
    }

    editPaid = b => {
      this.setState({
        editingPaid:b
      })
    }
    
    editFacturation = b => {
      this.setState({
        editingFacturation:b
      })
    }

    editDescription = _.debounce(()=>{
      this.props.client.mutate({
        mutation : this.state.setDescriptionQuery,
        variables:{
          _id:this.state.prestation._id,
          description:this.state.newDescription
        }
      })
    },300);

    setStep = newStep => {
      if(this.state.step == 1 && newStep == 1){
        newStep = 0
      }
      this.props.client.mutate({
        mutation : this.state.setStepQuery,
        variables:{_id:this.state.prestation._id,step:newStep}
      }).then(({data})=>{
        this.setState({
            step:data.setStep.step
        });
      })
    }
    
    setPaid = () => {
      this.props.client.mutate({
        mutation : this.state.setPaidQuery,
        variables:{_id:this.state.prestation._id,paid: Math.round(parseFloat(this.state.newPaid.toString().replace(",",".")) * 100) / 100}
      }).then(({data})=>{
        this.setState({
            paid:data.setPaid.paid,
            newPaid:data.setPaid.paid,
            editingPaid:false
        });
      })
    }
    
    setFacturation = () => {
      this.props.client.mutate({
        mutation : this.state.setFacturationQuery,
        variables:{_id:this.state.prestation._id,facturation:Math.round(parseFloat(this.state.newFacturation.toString().replace(",",".")) * 100) / 100}
      }).then(({data})=>{
        this.setState({
            facturation:data.setFacturation.facturation,
            newFacturation:data.setFacturation.facturation,
            editingFacturation:false
        });
      })
    }

    setDescription = () => {
      this.props.client.mutate({
        mutation : this.state.setDescriptionQuery,
        variables:{_id:this.state.prestation._id,description:this.state.newDescription}
      }).then(({data})=>{
        this.setState({
            description:data.setDescription.description,
            newDescription:data.setDescription.description
        });
      })
    }

    getPaidContent = () => {
      if(this.state.editingPaid){
        return (
          <Statistic style={{display:"grid",gridTemplateColumns:"70% 1fr",gridTemplateRows:"19px 86px",margin:"0"}} color="green">
            <Statistic.Label style={{gridColumnStart:"1",gridColumnEnd:"span 2",textAlign:"center",letterSpacing:"0.2em"}}>
              Montant réglé
            </Statistic.Label>
            <Input name="newPaid" onChange={this.handleChange} fluid className="inputTextGreen" style={{gridColumnStart:"1",gridRowStart:"2",alignSelf:"center",height:"48px"}} defaultValue={this.state.paid}/>
            <Button style={{gridColumnStart:"2",gridRowStart:"2",placeSelf:"center"}} onClick={this.setPaid} color="blue" animated='fade'>
              <Button.Content visible><Icon name='check' /></Button.Content>
              <Button.Content hidden>Valider</Button.Content>
            </Button>
          </Statistic>
        )
      }else{
        const val = this.formatCS(this.state.paid).toString().split(".");
        return (
          <Statistic style={{height:"115px",margin:"0"}} color="green">
              <Statistic.Label style={{textAlign:"center",letterSpacing:"0.2em"}}>
                Montant réglé
              </Statistic.Label>
              <Statistic.Value onClick={()=>{this.editPaid(true)}} style={{lineHeight:"64px",fontFamily: "'Merienda', sans-serif",fontSize:"3em",margin:"16px 0"}}>
                {val[0]} 
                <span style={{fontSize:"0.5em"}}>
                  {","+('00'+val[1]).slice(2)}
                </span> €
              </Statistic.Value>
          </Statistic>
        )
      }
    }

    getFacturationContent = () => {
      if(this.state.editingFacturation){
        return (
          <Statistic style={{display:"grid",gridTemplateColumns:"70% 1fr",gridTemplateRows:"19px 86px",margin:"0"}} color="blue">
              <Statistic.Label style={{gridColumnStart:"1",gridColumnEnd:"span 2",textAlign:"center",letterSpacing:"0.2em"}}>
                  Total facturé
              </Statistic.Label>
              <Input name="newFacturation" onChange={this.handleChange} fluid className="inputTextBlue" style={{gridColumnStart:"1",gridRowStart:"2",alignSelf:"center",height:"48px"}} defaultValue={this.state.facturation}/>
              <Button style={{gridColumnStart:"2",gridRowStart:"2",placeSelf:"center"}} onClick={this.setFacturation} color="blue" animated='fade'>
                <Button.Content visible><Icon name='check' /></Button.Content>
                <Button.Content hidden>Valider</Button.Content>
              </Button>
          </Statistic>
        )
      }else{
        const val = this.formatCS(this.state.facturation).toString().split(".");
        return (
          <Statistic style={{height:"115px",margin:"0"}} color="blue">
              <Statistic.Label style={{textAlign:"center",letterSpacing:"0.2em"}}>
                  Total facturé
              </Statistic.Label>
              <Statistic.Value onClick={()=>{this.editFacturation(true)}} style={{lineHeight:"64px",fontFamily: "'Merienda', sans-serif",fontSize:"3em",margin:"16px 0"}}>
                {val[0]} 
                <span style={{fontSize:"0.5em"}}>
                    {","+('00'+val[1]).slice(2)}
                </span> €
              </Statistic.Value>
          </Statistic>
        )
      }
    }

    formatCS = n =>{
      return parseFloat(Math.round(n * 100) / 100).toFixed(2);
    }

  render() {
    const { prestation,step } = this.state;
    if(prestation == null){
      return <p>Loading ...</p>
    }
    return (
      <div style={{display:"grid",gridTemplateColumns:"270px 1fr",gridTemplateRows:"136px 48px 88px 1fr 48px",gridGap:"16px"}}>
        <div style={{gridRowStart:"1",gridRowEnd:"span 2",gridColumnStart:"1",display:"flex",justifyContent:"center"}}>
          <img style={{cursor:"pointer",width:"160px",height:"160px",margin:"32px 0 0 0"}} src={"/res/avatar/"+ (prestation.client._id ? ('000'+prestation.client.avatar).slice(-3) : "000" ) +".png"} alt="active-client-avatar"/>
        </div>
        <Segment style={{gridRowStart:"3",gridColumnStart:"1",margin:"0"}} textAlign="center">
          <span style={{fontSize:"1.2em"}}>
            {(prestation.client._id ? (prestation.client.firstname + " " + prestation.client.lastname) : "Aucun client affecté")}
          </span>
          <br/>
          <span style={{color:"#777"}}>
            {prestation.client.phone}
            <br/>
            {prestation.client.mail}
          </span>
        </Segment>
        <Segment textAlign="center" style={{gridRowStart:"4",gridColumnStart:"1",heigth:"256px",margin:"0",placeSelf:"stretch"}}>
          {this.getPaidContent()}
          {this.getFacturationContent()}
        </Segment>
        <Button style={{gridRowStart:"5",gridColumnStart:"1",width:"100%"}} inverted onClick={()=>{this.removePrestation(prestation._id)}} color="red" animated='fade'>
          <Button.Content visible><Icon name='cancel' /></Button.Content>
          <Button.Content hidden>Supprimer cette prestation</Button.Content>
        </Button>
        <Step.Group style={{border:"0",margin:"32px 0 8px 0",padding:"3px",borderRadius:"8px",backgroundImage:"linear-gradient(315deg, #352384 25%, #F3F3F5 100%)"}} widths={5}>
          <Step onClick={()=>{this.setStep(1)}} completed={(step>0)}>
            <Icon name='wordpress forms' />
            <Step.Content>
              <Step.Title>Commande</Step.Title>
              <Step.Description>Les détails de la commande ont été transmit</Step.Description>
            </Step.Content>
          </Step>
          <Step onClick={()=>{this.setStep(2)}} completed={(step>1)}>
            <Icon name='credit card' />
            <Step.Content>
              <Step.Title>Payement</Step.Title>
              <Step.Description>La commande à été payée en totalité</Step.Description>
            </Step.Content>
          </Step>
          <Step onClick={()=>{this.setStep(3)}} completed={(step>2)}>
            <Icon name='shipping' />
            <Step.Content>
              <Step.Title>Livraison</Step.Title>
              <Step.Description>Le produit à été récéptionné par le client</Step.Description>
            </Step.Content>
          </Step>
          <Step onClick={()=>{this.setStep(4)}} completed={(step>3)}>
            <Icon name='archive' />
            <Step.Content>
              <Step.Title>Archivage</Step.Title>
              <Step.Description>La commande est cloturée</Step.Description>
            </Step.Content>
          </Step>
        </Step.Group>
        <Header style={{margin:"0 32px"}} as='h1'>
          <p style={{float:"left"}}>{prestation.name}</p>
          <p style={{float:"right"}}>{"commande pour le "+('00'+prestation.deliveryDay).slice(-2).toString()+"/"+('00'+prestation.deliveryMonth).slice(-2).toString()+"/"+prestation.deliveryYear}</p>
        </Header>
        <TextArea name="newDescription" onChange={e=>{this.handleChange(e);this.editDescription()}} defaultValue={prestation.description} style={{fontFamily:"Ubuntu",backgroundColor:"#d9e4f5",backgroundImage:"linear-gradient(315deg, #d9e4f5 0%, #f5e3e6 74%)",border:"0px",borderRadius:"4px",padding:"16px",fontSize:"1.6em",width:"100%",gridRowEnd:"span 3"}} autoHeight placeholder='Description de la commande ...'/>
      </div>
    )
  }
}

const withPrestationsContext = WrappedComponent => props => (
  <PrestationsContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </PrestationsContext.Consumer>
)
  
const wrappedPrestationContext = withPrestationsContext(Prestation);

const withRouterPrestation = withRouter(wrappedPrestationContext);

const removePrestation = gql`
    mutation removePrestation($_id:String!){
      removePrestation(_id:$_id)
    }
`;

export default compose(
  withApollo,
  graphql(removePrestation,{
      name:"removePrestation",
      options:{
          refetchQueries: () => [ "Prestations" ]
  }}))(withRouterPrestation);