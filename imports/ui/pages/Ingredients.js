import React, { Component, Fragment } from 'react';
import { Modal,Input,Table,Button,Form } from 'semantic-ui-react';
import IngredientRow from '../molecules/IngredientRow';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';

class Ingredients extends Component {

  state={
    IngredientsQuery : gql`
    query Ingredients{
      ingredients{
        _id
        name
        prix
        quantite
        unite
        allergenics
        fournisseur
      }
    }`,
    addIngredientQuery : gql`
      mutation addIngredient($name: String!, $prix: Float!,$quantite: Float!, $unite: String!, $fournisseur: String!){
      addIngredient(name: $name,prix: $prix,quantite: $quantite,unite: $unite,fournisseur: $fournisseur) {
          _id
          name
          prix
          quantite
          unite
          allergenics
          fournisseur
        }
      }
    `,
    editIngredientQuery : gql`
      mutation editIngredient($_id:String!,$name:String!,$prix:Float!,$quantite:Float!,$unite:String!,$fournisseur:String!){
        editIngredient(_id:$_id,name:$name,prix:$prix,quantite:$quantite,unite:$unite,fournisseur:$fournisseur){
          _id
          name
          prix
          quantite
          unite
          fournisseur
        }
      }
    `,
    removeIngredientQuery : gql`
      mutation removeIngredient($_id: String!){
        removeIngredient(_id: $_id){
          _id
          name
          prix
          quantite
          unite
          allergenics
          fournisseur
        }
      }
    `,
    setAllergenics : gql`mutation setAllergenics($_id: String!,$allergenics: [Boolean]!){
      setAllergenics(_id: $_id, allergenics: $allergenics){
          _id
          name
          prix
          quantite
          unite
          allergenics
          fournisseur
        }
    }`,
    ingredientsRaw:[],
    ingredientsFilter:null,
    newIngredient:"",
    ingredients : () => {
      if(this.state.ingredientsFilter === null){
        return this.state.ingredientsRaw
      }else{
        return this.state.ingredientsRaw.filter(c => c.name.toLowerCase().includes(this.state.ingredientsFilter.toLowerCase()) || c.fournisseur.toLowerCase().includes(this.state.ingredientsFilter.toLowerCase()))
      }
    }
  }

  setAllergenics = ({_id,allergenics}) => {
    this.props.client.mutate({
      mutation:this.state.setAllergenics,
      variables:{ _id:_id,allergenics:allergenics},
    }).then(({data})=>{
        this.setState({
          ingredientsRaw:data.setAllergenics
        })
    })
  }

  handleChange = e => {
    this.setState({
        [e.target.name] : e.target.value
    })
  }

  addIngredient = () => {
    this.props.client.mutate({
      mutation : this.state.addIngredientQuery,
      variables:{
        name:this.state.newIngredient,
        prix:0,
        quantite:0,
        unite:"g",
        fournisseur:""
      }
    }).then( ({data}) =>{
      this.close();
      this.setState({
        ingredientsRaw:data.addIngredient
      })
    })
  }

  removeIngredient = _id => {
    this.props.client.mutate({
      mutation : this.state.removeIngredientQuery,
      variables:{
        _id:_id
      }
    }).then(({data}) =>{
      this.close();
      this.setState({
        ingredientsRaw:data.removeIngredient
      })
    })
  }

  editIngredient = variables => {
    this.props.client.mutate({
      mutation:this.state.editIngredientQuery,
      variables:variables
    }).then(({data})=>{
      this.loadIngredients();
    })
  }

  popModal = () => {
    this.setState({
        open:true
    })
  }

  close = () => {
    this.setState({
        open:false
    })
  }

  handleFilter= e => {
    this.setState({
      ingredientsFilter : e.target.value
    })
  }

  loadIngredients = () => {
    this.props.client.query({
      query: this.state.IngredientsQuery
    }).then(({data}) => {
      this.setState({
        ingredientsRaw:data.ingredients
      })
    })
  }

  componentDidMount = () => {
    this.loadIngredients()
  }

  componentDidUpdate = () => {
    if(this.state.open){
      document.querySelector('#newIngredientName').focus();
    }
  }

  render() {
    return (
    <Fragment>
      <div style={{display:"flex",justifyContent:"center",marginBottom:"32px"}}>
        <Input style={{width:"480px"}} name="filterClients" onChange={this.handleFilter} size='massive' icon='search' placeholder='Rechercher un ingredient ...' />
        <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.popModal()}} content='Ajouter un ingredient' icon='plus' labelPosition='right'/>
      </div>
      <div style={{margin:"32px 0 0 0"}}>
        <div style={{display:"block",overflowY:"auto",height:"735px"}} width={16}>
          <Table striped compact>
            <Table.Header>
              <Table.Row textAlign='center'>
                <Table.HeaderCell width={4}>Ingredient</Table.HeaderCell>
                <Table.HeaderCell width={4}>Fournisseur</Table.HeaderCell>
                <Table.HeaderCell width={4}>Allergènes</Table.HeaderCell>
                <Table.HeaderCell width={1}>Prix</Table.HeaderCell>
                <Table.HeaderCell width={1}>Quantité</Table.HeaderCell>
                <Table.HeaderCell width={2}>Actions</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {this.state.ingredients().map(i => (
                <IngredientRow editIngredient={this.editIngredient} setAllergenics={this.setAllergenics} removeIngredient={this.removeIngredient} key={i._id} ingredient={i}/>
              ))}
            </Table.Body>
          </Table>
        </div>
      </div>
      <Modal style={{backgroundColor:"rgba(0,0,0,0.6)",borderRadius:"16px",padding:"16px"}} basic size="large" open={this.state.open} onClose={this.close}>
        <Modal.Header>
          Ajout d'un nouvel ingredient :
        </Modal.Header>
        <Modal.Content >
          <Form onSubmit={e=>{e.preventDefault();this.addIngredient()}}>
            <Form.Input id="newIngredientName" size="big" label='Nom' placeholder='Nom' name="newIngredient" onChange={this.handleChange}/>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.addIngredient()}} content='Ajouter' icon='plus' labelPosition='right'/>
        </Modal.Actions>
      </Modal>
    </Fragment>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default withUserContext(Ingredients);