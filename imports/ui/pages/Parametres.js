import React, { Component } from 'react';
import { UserContext } from '../../contexts/UserContext';
import AccountRow from '../molecules/AccountRow';
import { Table,Button,Segment } from 'semantic-ui-react';
import gql from 'graphql-tag';

class Parametres extends Component {

  state = {
    users:this.props.users,
    usersQuery:gql`
      query Users{
        users{
          _id
          email
          isAdmin
          firstname
          lastname
          activated
        }
      }
    `,
    deleteAccountQuery:gql`mutation deleteAccount($admin: String!,$_id: String!){
      deleteAccount(admin: $admin,_id: $_id){
        _id
        email
        isAdmin
        firstname
        lastname
        activated
      }
    }`,
    resetQuery:gql`mutation reset($all:Boolean){
      reset(all:$all)
    }`,
    addPropQuery:gql`mutation addProp($content:String!){
      addProp(content:$content)
    }`,
    accounts : () => {
      return this.state.users.map(u=>(
        <AccountRow deleteAccount={this.deleteAccount} key={u._id} u={u}/>
      ))
    },
    saveDBQuery : gql`
    query saveDB{
      saveDB{
        ingredients{
          _id
          name
          prix
          quantite
          unite
          allergenics
          fournisseur
        }
        vIngredients{
          _id
          ingredient{
            _id
          }
          usedQ
          usedU
          usedP
        }
        recettes{
          _id
          name
          variations{
            _id
          }
        }
        variations{
          _id
          name
          vingredients{
            _id
          }
          
        }
        clients{
          _id
          firstname
          lastname
          avatar
          phone
          mail
          address
          zipcode
          city
        }
        prestations{
          _id
          name
          client{
            _id
          }
          deliveryDay
          deliveryMonth
          deliveryYear
          description
          step
          facturation
          paid
          archived
        }
        tasks{
          _id
          content
          completed
        }
      }
    }`
  }

  deleteAccount = _id =>{
    this.props.client.mutate({
      mutation : this.state.deleteAccountQuery,
      variables:{
          admin:Meteor.userId(),
          _id:_id
        }
    }).then(({data})=>{
      this.setState({
        users:data.deleteAccount
      });
    })
  }

  downloadDB = () => {
    this.props.client.query({
      query:this.state.saveDBQuery,
      fetchPolicy:"network-only"
    }).then((data)=>{
      let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(data.data.saveDB));
      let downloadAnchorNode = document.createElement('a');
      downloadAnchorNode.setAttribute("href",     dataStr);
      downloadAnchorNode.setAttribute("download", "ecrinDB" + new Date().getFullYear() + (new Date().getMonth()+1).toString() + new Date().getDate() + ".json");
      document.body.appendChild(downloadAnchorNode);
      downloadAnchorNode.click();
      downloadAnchorNode.remove();
    })
  }
  
  reset = () => {
    this.props.client.mutate({
      mutation:this.state.resetQuery,
      variables:{
        all:true
      }
    });
  }

  addProp = () => {
    this.props.client.mutate({
      mutation:this.state.addPropQuery,
      variables:{
        content:""
      }
    });
  }

  render() {
    return (
      <div style={{display:"grid",gridTemplateColumns:"1fr 2fr",gridTemplateRows:"320px auto",gridGap:"32px"}}>
        <Segment style={{gridColumnStart:"1"}}>
          <Button onClick={this.downloadDB}>Sauvegarder la base de données</Button>
        </Segment>
        <Table style={{margin:"0",gridColumnStart:"2",gridRowEnd:"span 2",justifySelf:"stretch",alignSelf:"start"}} compact selectable striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width={4}>Nom</Table.HeaderCell>
              <Table.HeaderCell width={4}>E-mail</Table.HeaderCell>
              <Table.HeaderCell style={{textAlign:"center"}} width={2}>Admin</Table.HeaderCell>
              <Table.HeaderCell style={{textAlign:"center"}} width={2}>Actif</Table.HeaderCell>
              <Table.HeaderCell style={{textAlign:"center"}} width={4}></Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {this.state.accounts()}
          </Table.Body>
        </Table>
      </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(Parametres);
