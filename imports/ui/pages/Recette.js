import React, { Component, Fragment } from 'react'
import { Grid, Header, Menu, Icon, Modal, Button, Form, Segment } from 'semantic-ui-react';
import { graphql,compose,withApollo } from 'react-apollo';
import RecetteVariation from '../subPages/RecetteVariation';
import gql from 'graphql-tag';

export class Recette extends Component {

    state ={
        variationsByRecette : gql`query variationsByRecette($recette: String!){
            variationsByRecette(recette: $recette) {
                _id
                name
            }
        }`,
        setIngredientQuery: gql`mutation setIngredientQuery($_id: String!, $isIngredient: Boolean!){
            setIngredient(_id: $_id, isIngredient: $isIngredient) {
                _id
                isIngredient
            }
        }`,
        _id:this.props.match.params.id,
        newVariationName:"",
        name:null,
        isIngredient:null,
        variations:[],
        activeVariationId:null,
        open: false
    }

    handleVariationSelection = _id => {
        this.setState({
            activeVariationId :_id
        })
    }

    handleChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    getRecetteById = _id => {
        this.props.client.query({
            query : gql`
                query Recette($_id: String!) {
                    recette(_id: $_id) {
                        _id
                        name
                        isIngredient
                    }
                }
            `,
            variables:{_id:_id}
        }).then(({data})=>{
            this.setState({
                _id:data.recette._id,
                name:data.recette.name,
                isIngredient:data.recette.isIngredient
            });
        })
    }

    getVariations = () => {
        this.props.client.query({
            query:this.state.variationsByRecette,
            variables:{ recette:this.state._id },
            fetchPolicy:"network-only"
        }).then(({data})=>{
            this.setState({
                variations:data.variationsByRecette
            }) 
        })
    }

    addVariation = () => {
        this.props.addVariation({
            variables:{
                name:this.state.newVariationName,
                recette:this.state._id
            }
        }).then(()=>{
            this.props.client.query({
                query:this.state.variationsByRecette,
                variables:{ recette:this.state._id },
                fetchPolicy:"network-only"
            }).then(({data})=>{
                this.setState({
                    variations:data.variationsByRecette
                }) 
            })
        })
        this.close();
    }

    removeVariation = () => {
        if(this.state.activeVariationId != null){
            this.props.removeVariation({
                variables:{
                    _id:this.state.activeVariationId,
                }
            }).then(()=>{
                this.props.client.query({
                    query:this.state.variationsByRecette,
                    variables:{ recette:this.state._id },
                    fetchPolicy:"network-only"
                }).then(({data})=>{
                    this.setState({
                        variations:data.variationsByRecette,
                        activeVariationId:data.variationsByRecette[0]._id || null
                    }) 
                })
            })
        }
    }

    popModal = () => {
        this.setState({
            open:true
        })
    }

    close = () => {
        this.setState({
            open:false
        })
    }

    componentDidMount = () => {
        this.getRecetteById(this.state._id);
        this.getVariations();
    }
    
  render() {
    const { name,variations,activeVariationId } = this.state;
    if(variations.length != 0 && activeVariationId == null){this.setState({activeVariationId:variations[0]._id})}
    if((variations.length == 0 && activeVariationId == null) || (activeVariationId == null)){
        return (
            <Fragment>
                <Header style={{display:"inline-block",position:"relative",top:"7px"}} as="h1">
                    {name}
                </Header>
                <Segment style={{padding:"8px 14px"}}>
                    <Menu secondary>
                        {variations.map(v => (
                            <Menu.Item color="blue" style={{padding:"8px 14px",fontSize:"1.1em"}} key={v._id} name={v._id} active={activeVariationId === v._id} onClick={()=>{this.handleVariationSelection(v._id)}}>
                                {v.name}
                            </Menu.Item>
                        ))}
                        <Menu.Menu position='right'>
                            <Menu.Item key="addVariation" name="addVariation" color="blue" active={true} onClick={this.popModal}>
                                <Icon style={{margin:"0"}} name="plus"/>
                            </Menu.Item>
                        </Menu.Menu>
                    </Menu>
                </Segment>
                <Modal style={{backgroundColor:"rgba(0,0,0,0.6)",borderRadius:"16px",padding:"16px"}} basic size="large" open={this.state.open} onClose={this.close} >
                    <Modal.Header>
                        Ajout d'une nouvelle version à cette recette :
                    </Modal.Header>
                    <Modal.Content >
                        <Form>
                            <Grid>
                                <Grid.Column width={8}>
                                    <Form.Input size="big" labelPosition="left" icon='tag' label='Nom' placeholder='Nom' name="newVariationName" onChange={this.handleChange}/>
                                </Grid.Column>
                            </Grid>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={this.addVariation} content='Créer' icon='plus' labelPosition='right'/>
                    </Modal.Actions>
                </Modal>
            </Fragment>
        )
    }else{
        return (
            <Fragment>
                <div style={{display:'flex',justifyContent:'space-between'}}>
                    <Header style={{display:"inline-block",position:"relative",top:"7px"}} as="h1">
                        {name}
                    </Header>
                </div>
                <Segment style={{padding:"8px 14px"}}>
                    <Menu secondary>
                        {variations.map(v => (
                            <Menu.Item color="blue" style={{padding:"8px 14px",fontSize:"1.1em"}} key={v._id} name={v._id} active={activeVariationId === v._id} onClick={()=>{this.handleVariationSelection(v._id)}}>
                                {v.name}
                            </Menu.Item>
                        ))}
                        <Menu.Menu position='right'>
                            <Menu.Item key="addVariation" color="blue" active={true} name="addVariation" onClick={this.popModal}>
                                <Icon style={{margin:"0"}} name="plus"/>
                            </Menu.Item>
                            <Menu.Item key="removeVariation" color="red" active={true} name="addVariation" onClick={this.removeVariation}>
                                <Icon style={{margin:"0"}} name="cancel"/>
                            </Menu.Item>
                        </Menu.Menu>
                    </Menu>
                </Segment>
                <RecetteVariation recipeName={this.state.name} removeVariation={this.removeVariation} activeVariationId={activeVariationId} />
                <Modal style={{backgroundColor:"rgba(0,0,0,0.6)",borderRadius:"16px",padding:"16px"}} basic size="large" open={this.state.open} onClose={this.close} >
                    <Modal.Header>
                        Ajout d'une nouvelle version à cette recette :
                    </Modal.Header>
                    <Modal.Content >
                        <Form>
                            <Grid>
                                <Grid.Column width={8}>
                                    <Form.Input size="big" labelPosition="left" icon='tag' label='Nom' placeholder='Nom' name="newVariationName" onChange={this.handleChange}/>
                                </Grid.Column>
                            </Grid>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={this.addVariation} content='Créer' icon='plus' labelPosition='right'/>
                    </Modal.Actions>
                </Modal>
            </Fragment>
        )
    }
  }
}

const addVariation = gql`
    mutation addVariation($name: String!,$recette: String!){
        addVariation(name: $name, recette: $recette) {
            _id
            name
        }
    }
`;

const removeVariation = gql`
    mutation removeVariation($_id: String!){
        removeVariation(_id: $_id)
    }
`;

export default compose(
    withApollo,
    graphql(addVariation,{
        name:"addVariation",
        options:{
            refetchQueries: () => [ "Recette" ]
    }}),
    graphql(removeVariation,{
        name:"removeVariation",
        options:{
            refetchQueries: () => [ "Recette" ]
    }})
)(Recette);