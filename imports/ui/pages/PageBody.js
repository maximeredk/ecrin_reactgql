import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom';
import { UserContext } from '../../contexts/UserContext';
import Home from './Home';
import Ingredients from './Ingredients';
import Recettes from './Recettes';
import Recette from './Recette';
import Clients from './Clients';
import Client from './Client';
import Prestations from './Prestations';
import Prestation from './Prestation';
import Parametres from './Parametres';

class PageBody extends Component {

  render() {
    return (
      <div style={{
        width:"calc(100vw - 180px)",
        margin:"0 0 0 180px",
        padding:"32px 64px 16px 64px",
        display:"inline-block",
        linearGradient:"(315deg, #fffsff 0%, #d1e12c 74%)",
        backgroundImage:"url('/res/wall/backg"+6+".png')",
        backgroundRepeat:"no-repeat",
        backgroundAttachment:"fixed",
        minHeight:"100vh"
      }}>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/ingredients' component={Ingredients}/>
          <Route exact path='/recettes' component={Recettes}/>
          <Route exact path='/recette/:id' component={Recette}/>
          <Route exact path='/clients' component={Clients}/>
          <Route exact path='/client/:id' component={Client}/>
          <Route exact path='/prestations/:y/:m' component={Prestations}/>
          <Route exact path='/prestation/:id' component={Prestation}/>
          <Route exact path='/parametres' component={Parametres}/>
          <Redirect from='*' to={'/'}/>
        </Switch>
      </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default withUserContext(PageBody);