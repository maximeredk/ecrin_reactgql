import React, { Component } from 'react';
import { Header, Image } from 'semantic-ui-react';
import CalendarTile from "../molecules/CalendarTile";
import { withRouter } from 'react-router-dom';
import { ClientsContext } from '../../contexts/ClientsContext';
import { graphql, Query, withApollo } from 'react-apollo';
import gql from 'graphql-tag';


class Calendar extends Component {
  state={
    y : parseInt(this.props.match.params.y),
    m : parseInt(this.props.match.params.m),
    clientsMappedList : [],
    selected:null,
    DaysQuery : gql`query getPrestationsByDays($m: Int!,$y: Int!) {
      getDaysInMonth(m:$m,y:$y){
        key
        prestations{
          _id
          name
          client{
            _id
            firstname
            lastname
            avatar
          }
          deliveryDay
          deliveryMonth
          deliveryYear
          facturation
          paid
          archived
          step
        }
        day
        month
        year
        dow
        today
      }
    }`
  }

  addPrestation = ({client,dDay,dMonth,dYear,name}) => {
    this.props.addPrestation({
        variables:{
          name:name,
          client:client,
          deliveryDay:dDay,
          deliveryMonth:dMonth,
          deliveryYear:dYear,
          facturation:0,
          paid:0,
          archived:false
        }
    }).then( ({data}) =>{
      this.props.history.push("/prestation/"+data.addPrestation._id);
    })
  }

  dateIsSelected = ({ d,m,y }) => {
    const date = new Date (y,m,d);
    if(this.state.selected != null){
      if(date.getDate() == this.state.selected.getDate() && date.getMonth() == this.state.selected.getMonth() && date.getFullYear() == this.state.selected.getFullYear()){
        return true
      }else{
        return false
      }
    }else{
      return false;      
    }
  }

  getMonthName = n =>{
    return ["Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre"][n-1];
  }

  navigateToPrevMonth = () => {
    let { y,m } = this.state;
    if(m<=1){
      y=y-1;
      m=12;
    }else{
      m=m-1;
    }
    this.props.history.push("/prestations/"+y+"/"+m)
    this.setState({y:y,m:m})
  }

  navigateToNextMonth = () => {
    let { y,m } = this.state;
    if(m>=12){
      y=y+1;
      m=1;
    }else{
      m=m+1;
    }
    this.props.history.push("/prestations/"+y+"/"+m)
    this.setState({y:y,m:m})
  }

  selectDate = (y,m,d) =>{
    this.setState({
      selected:new Date(y,m,d)
    })
    this.props.loadPrestationOfDay({y:y,m:m,d:d});
  }

  componentDidMount = () => {
    this.props.client.query({
      query: gql`query Clients{
        clients{
          _id
          firstname
          lastname
          avatar
        }
      }`
    }).then(({data}) => {
      this.setState({
        clientsMappedList:data.clients.map(x => { return {value:x._id,text:x.lastname+", "+x.firstname,image:{src: "/res/avatar/"+ ('000'+x.avatar).slice(-3) +".png"}}})
      })
    })
  }

  render() {
    const { m,y,clientsMappedList } = this.state;
    return (
      <div>
        <div style={{margin:"16px auto 16px auto",width:"338px"}}>
          <Image onClick={()=>{this.navigateToPrevMonth()}} style={{cursor:"pointer",width:"32px",height:"32px",margin:"0 16px",transform:"rotate(180deg)",display:"inline-block"}} src="/res/right-chevron.png"/>
          <div style={{width:"210px",padding:"auto",display:"inline-block"}}>
            <Header as="h1" textAlign='center' style={{margin:"0 auto",position:"relative",top:"7px"}} as="h1">{this.getMonthName(m)+" "+y}</Header>
          </div>
          <Image onClick={()=>{this.navigateToNextMonth()}} style={{cursor:"pointer",width:"32px",height:"32px",margin:"0 16px",display:"inline-block"}} src="/res/right-chevron.png"/>
        </div>
        <div style={{margin:"16px auto 16px auto",display:"grid",gridGap:"8px",gridTemplateColumns:"1fr 1fr 1fr 1fr 1fr 1fr 1fr",gridTemplateRows:"120px 120px 120px 120px 120px 120px"}}>
          <Query fetchPolicy={"network-only"} query={this.state.DaysQuery} variables={{ m:parseInt(this.props.match.params.m), y:parseInt(this.props.match.params.y) }}>
            {({ loading, error, data }) => {
              if (loading) return null;
              return data.getDaysInMonth.map(day => (
                <CalendarTile key={day.key} addPrestation={this.addPrestation} clientsMappedList={clientsMappedList} selected={this.dateIsSelected({d:day.day,m:day.month,y:day.year})} day={day} selectDate={this.selectDate} />
              ))
            }}
          </Query>
        </div>
      </div>
    )
  }
}

const withClientsContext = WrappedComponent => props => (
  <ClientsContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
  </ClientsContext.Consumer>
)

const wrappedClientsContext = withClientsContext(Calendar);

const withRouterContext = withRouter(wrappedClientsContext);

const addPrestation = gql`
  mutation addPrestation($name:String!,$client:String,$deliveryDay:Int!,$deliveryMonth:Int!,$deliveryYear:Int!,$facturation:Float!,$paid:Float!,$archived:Boolean!){
    addPrestation(name:$name,client:$client,deliveryDay:$deliveryDay,deliveryMonth:$deliveryMonth,deliveryYear:$deliveryYear,facturation:$facturation,paid:$paid,archived:$archived) {
      _id
      name
      client{
        _id
        firstname
        lastname
        avatar
        phone
        mail
        address
        zipcode
        city
      }
      deliveryDay
      deliveryMonth
      deliveryYear
      facturation
      paid
      archived
    }
  }
`;

export default graphql(addPrestation,{
  name:"addPrestation",
  options:{
      refetchQueries: () => [ "Prestations","getPrestationsByDays" ]
  }
})(withApollo(withRouterContext))