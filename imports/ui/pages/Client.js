import React, { Component } from 'react';
import { Form, Grid, GridColumn, Button, Icon } from 'semantic-ui-react';
import { ClientsContext } from '../../contexts/ClientsContext';
import { AvatarPicker } from '../molecules/AvatarPicker';
import { withRouter } from 'react-router-dom';
import { graphql,compose,withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import _ from 'lodash';

export class Client extends Component {

    state ={
        _id:this.props.match.params.id,
        firstname:null,
        lastname:null,
        avatar:null,
        phone:null,
        mail:null,
        address:null,
        zipcode:null,
        city:null
    }

    getClientById = _id => {
        this.props.client.query({
            query : gql`
                query Client($_id: String!) {
                    client(_id: $_id) {
                        _id
                        firstname
                        lastname
                        avatar
                        phone
                        mail
                        address
                        zipcode
                        city
                    }
                }
            `,
            variables:{_id:_id}
        }).then(({data})=>{
            this.setState({
                firstname:data.client.firstname,
                lastname:data.client.lastname,
                avatar:data.client.avatar,
                phone:data.client.phone,
                mail:data.client.mail,
                address:data.client.address,
                zipcode:data.client.zipcode,
                city:data.client.city
            });
        })
    }

    removeClient = _id => {
        this.props.removeClient({
            variables:{
                _id:_id
            }
        });
        this.props.history.push("/clients");
    }

    componentDidMount = () => {
        this.getClientById(this.state._id);
    }

    handleChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    editAvatar = id => {
        this.setState({
            avatar:id
        })
        this.handleEdit();
    }

    randomAvatar = () => {
        this.setState({
            avatar:Math.floor(Math.random()*260)+1
        })
        this.handleEdit();
    }

    handleEdit = _.debounce(()=>{
        this.props.editClient({
            variables:{
                _id:this.state._id,
                firstname:this.state.firstname,
                lastname:this.state.lastname,
                avatar:this.state.avatar,
                phone:this.state.phone,
                mail:this.state.mail,
                address:this.state.address,
                zipcode:this.state.zipcode,
                city:this.state.city
            }
        })
    },400);

  render() {
    const { _id,firstname,lastname,avatar,phone,mail,address,zipcode,city } = this.state;
    if(avatar == null){
        return <p>Loading ...</p>
    }
    return (
        <Grid style={{marginTop:"32px"}}>
            <GridColumn style={{display:"grid",justifyItems:"center",gridTemplateRows:"40px 224px 36px 1fr",gridTemplateColumns:"1fr 128px 1fr",marginTop:"12px"}} width={3}>
                <Button inverted style={{gridColumnStart:"2",width:"100%"}} onClick={()=>{this.removeClient(_id)}} color="red" animated='fade'>
                    <Button.Content visible><Icon name='cancel' /></Button.Content>
                    <Button.Content hidden>Supprimer</Button.Content>
                </Button>
                <AvatarPicker editAvatar={this.editAvatar} avatar={avatar}/>
                <Button inverted style={{gridColumnStart:"2",width:"100%"}} onClick={this.randomAvatar} secondary animated='fade'>
                    <Button.Content visible><Icon name='random'/></Button.Content>
                    <Button.Content hidden>Aléatoire</Button.Content>
                </Button>
            </GridColumn>
            <GridColumn width={13}>
                <Form>
                    <div style={{backgroundColor:"rgba(0,0,0,0.6)",padding:"32px"}}>
                        <Grid stackable>
                            <Grid.Column width={6}>
                                <Form.Input size="big" label='Prénom' placeholder='Prénom' defaultValue={firstname} name='firstname' onChange={e=>{this.handleChange(e);this.handleEdit()}}/>
                                <Form.Input size="big" label='Nom' placeholder='Nom'defaultValue={lastname} name='lastname' onChange={e=>{this.handleChange(e);this.handleEdit()}}/>
                            </Grid.Column>
                            <Grid.Column width={10}>
                                <Form.Input size="big" labelPosition="left" icon='phone' label='Telephone' placeholder='Telephone' defaultValue={phone} name='phone' onChange={e=>{this.handleChange(e);this.handleEdit()}}/>
                                <Form.Input size="big" labelPosition="left" icon='mail' label='Mail' placeholder='Mail' defaultValue={mail} name='mail' onChange={e=>{this.handleChange(e);this.handleEdit()}}/>
                            </Grid.Column>
                        </Grid>
                        <Grid stackable>
                            <Grid.Column floated="right" width={12}>
                                <Form.Input size="big" labelPosition="left" icon='home' label='Adresse' placeholder='Adresse' defaultValue={address} name='address' onChange={e=>{this.handleChange(e);this.handleEdit()}}/>
                                <Form.Group>
                                    <Form.Input size="big" labelPosition="left" icon='building' label='Commune' placeholder='Commune' defaultValue={city} name='city' width={12} onChange={e=>{this.handleChange(e);this.handleEdit()}}/>
                                    <Form.Input size="big" labelPosition="left" icon='barcode' label='Code Postal' placeholder='Code Postal' defaultValue={zipcode} name='zipcode' width={4} onChange={e=>{this.handleChange(e);this.handleEdit()}}/>
                                </Form.Group>
                            </Grid.Column>
                        </Grid>
                    </div>
                </Form>
            </GridColumn>
        </Grid>
    )
  }
}

  const withClientContext = WrappedComponent => props => (
    <ClientsContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </ClientsContext.Consumer>
  )
  
  const wrappedClientContext = withClientContext(Client);
  
  const withRouterClient = withRouter(wrappedClientContext);

    const removeClient = gql`
        mutation removeClient($_id:String!){
            removeClient(_id:$_id)
        }
    `;

    const editClient = gql`
        mutation editClient($_id:String!,$firstname:String!,$lastname:String!,$avatar:Int!,$phone:String!,$mail:String!,$address:String!,$zipcode:String!,$city:String!){
            editClient(_id:$_id,firstname:$firstname,lastname:$lastname,avatar:$avatar,phone:$phone,mail:$mail,address:$address,zipcode:$zipcode,city:$city){
                _id
                firstname
                lastname
                avatar
                phone
                mail
                address
                zipcode
                city
            }
        }
    `;

  export default compose(
    withApollo,
    graphql(removeClient,{
        name:"removeClient",
        options:{
            refetchQueries: () => [ "Clients" ]
    }}),graphql(editClient,{
        name:"editClient",
        options:{
            refetchQueries: () => [ "Clients" ]
    }}),
    )(withRouterClient);