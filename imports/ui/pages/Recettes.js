import React, { Component, Fragment } from 'react';
import { Header,Grid,Table,Button,Modal,Form } from 'semantic-ui-react';
import RecetteRow from '../molecules/RecetteRow';
import { graphql, Query } from 'react-apollo';
import gql from 'graphql-tag';

class Recettes extends Component {

  state={
    newRecetteName:"",
    RecettesQuery : gql`
    query Recettes{
      recettes{
        _id
        name
      }
    }`
  }

  popModal = () => {
    this.setState({
        open:true
    })
  }

  close = () => {
    this.setState({
        open:false
    })
  }

  handleChange = e => {
    this.setState({
        [e.target.name] : e.target.value
    })
  }

  addRecette = () => {
    this.close();
    this.props.addRecette({
        variables:{
            name:this.state.newRecetteName,
        }
    });
  }

  render() {
    return (
      <Fragment>
        <div style={{margin:"32px 0 0 0"}}>
          <Grid>
            <Grid.Column width={16}>
              <Table striped compact>
                <Table.Header>
                  <Table.Row textAlign='center'>
                    <Table.HeaderCell width={14}>Recette</Table.HeaderCell>
                    <Table.HeaderCell width={2}>Actions</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  <Query query={this.state.RecettesQuery}>
                    {({ loading, error, data }) => {
                      if (loading) return null;
                      return data.recettes.map(r => (
                        <RecetteRow key={r._id} recette={r} />
                      ))
                    }}
                  </Query>
                  <Table.Row>
                    <Table.Cell></Table.Cell>
                    <Table.Cell style={{padding:"12px 0",display:"flex",justifyContent:"center",alignItems:"center"}}>
                      <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={this.popModal} content='Créer' icon='plus' labelPosition='left'/>
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            </Grid.Column>
          </Grid>
        </div>
        <Modal style={{backgroundColor:"rgba(0,0,0,0.6)",borderRadius:"16px",padding:"16px"}} basic size="large" open={this.state.open} onClose={this.close} >
          <Modal.Header>
            Ajout d'une nouvelle recette :
          </Modal.Header>
          <Modal.Content >
            <Form>
              <Grid>
                <Grid.Column width={8}>
                  <Form.Input size="big" labelPosition="left" icon='tag' label='Nom' placeholder='Nom' name="newRecetteName" onChange={this.handleChange}/>
                </Grid.Column>
              </Grid>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={this.addRecette} content='Créer' icon='plus' labelPosition='right'/>
          </Modal.Actions>
        </Modal>
      </Fragment>
    )
  }
}

const addRecette = gql`
    mutation addRecette($name: String!){
      addRecette(name: $name) {
            _id
            name
        }
    }
`;

export default graphql(addRecette,{
    name:"addRecette",
    options:{
        refetchQueries: () => [ "Recettes" ]
    }
})(Recettes)