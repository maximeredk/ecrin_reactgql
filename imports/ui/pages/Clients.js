import React, { Component } from 'react';
import { Input, Button, Modal, Form, Grid } from 'semantic-ui-react';
import { ClientsContext } from '../../contexts/ClientsContext';
import ClientTile from '../molecules/ClientTile';
import { withRouter } from 'react-router-dom';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

class Clients extends Component {
  state={
    clientsRaw: this.props.clients,
    clientsFilter:null,
    firstname:"",
    lastname:"",
    phone:"",
    mail:"",
    clients : () => {
      if(this.state.clientsFilter === null){
        return this.props.clients
      }else{
        return this.props.clients.filter(c => c.firstname.toLowerCase().includes(this.state.clientsFilter.toLowerCase()) || c.lastname.toLowerCase().includes(this.state.clientsFilter.toLowerCase()))
      }
    }
  }

  handleChange = e => {
    this.setState({
        [e.target.name] : e.target.value
    })
  }

  addClient = () => {
    this.props.addClient({
        variables:{
          firstname:this.state.firstname,
          lastname:this.state.lastname,
          avatar:Math.floor(Math.random() * 260) + 1,
          phone:this.state.phone,
          mail:this.state.mail,
          address:"",
          zipcode:"",
          city:"",
        }
    }).then( ({data}) =>{
      this.close();
      this.props.history.push("/client/"+data.addClient._id);
    })
  }

  popModal = () => {
    this.setState({
        open:true
    })
  }

  close = () => {
    this.setState({
        open:false
    })
  }

  handleFilter= e => {
    this.setState({
      clientsFilter : e.target.value
    })
  }

  render() {
    return (
      <div>
        <div style={{display:"flex",justifyContent:"center",marginBottom:"32px"}}>
          <Input name="filterClients" onChange={this.handleFilter} size='massive' icon='search' placeholder='Rechercher un client ...' />
          <Modal style={{backgroundColor:"rgba(0,0,0,0.6)",borderRadius:"16px",padding:"16px"}} basic size="large" open={this.state.open} onClose={this.close} trigger={
            <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.popModal()}} content='Ajouter un client' icon='plus' labelPosition='right'/>
          }>
            <Modal.Header>
              Ajout d'un nouveau client :
            </Modal.Header>
            <Modal.Content >
              <Form>
                <Grid>
                  <Grid.Column width={8}>
                    <Form.Input size="big" label='Prénom' placeholder='Prénom' name="firstname" onChange={this.handleChange}/>
                    <Form.Input size="big" label='Nom' placeholder='Nom' name="lastname" onChange={this.handleChange}/>
                  </Grid.Column>
                  <Grid.Column width={8}>
                    <Form.Input size="big" labelPosition="left" icon='phone' label='Telephone' placeholder='Telephone' name="phone" onChange={this.handleChange}/>
                    <Form.Input size="big" labelPosition="left" icon='mail' label='Mail' placeholder='Mail' name="mail" onChange={this.handleChange}/>
                  </Grid.Column>
                </Grid>
              </Form>
            </Modal.Content>
            <Modal.Actions>
              <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.addClient()}} content='Ajouter' icon='plus' labelPosition='right'/>
            </Modal.Actions>
          </Modal>
        </div>
        <div style={{display:"flex",flexWrap:"wrap",justifyContent:"space-around",width:"100%"}}>
          {this.state.clients().map(c =>(
            <ClientTile key={c._id} client={c}/>
          ))}
        </div>
      </div>
    )
  }
}

const withClientContext = WrappedComponent => props => (
  <ClientsContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </ClientsContext.Consumer>
)

const wrappedClientContext = withClientContext(Clients);

const withRouterClient = withRouter(wrappedClientContext);

const addClient = gql`
    mutation addClient($firstname:String!,$lastname:String!,$avatar:Int!,$phone:String!,$mail:String!,$address:String!,$zipcode:String!,$city:String!){
      addClient(firstname:$firstname,lastname:$lastname,avatar:$avatar,phone:$phone,mail:$mail,address:$address,zipcode:$zipcode,city:$city) {
          _id
          firstname
          lastname
          avatar
          phone
          mail
          address
          zipcode
          city
        }
    }
`;

export default graphql(addClient,{
    name:"addClient",
    options:{
        refetchQueries: () => [ "Clients" ]
    }
})(withRouterClient)