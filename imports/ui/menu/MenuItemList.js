import React, { Fragment,Component } from 'react'
import { Link } from 'react-router-dom';

class MenuItemList extends Component {
  render() {
    const { menuItems } = this.props;
    const list = menuItems.map((item) =>
        <Fragment key={item.name}>
          <Link to={'/'+ item.name} style={{ textDecoration: 'none' }}>
          <li style={{cursor:"pointer",marginTop:"32px"}} name={item.name}>
            <img style={{width:"56px",height:"56px"}} src={"/res/menu/"+item.label.toLowerCase()+".png"} alt="menuItemLogo"/>
            <p style={{
                marginTop:"4px",
                fontSize:"1.5em",
                backgroundColor:"#2a2a72",
                backgroundImage: "linear-gradient(315deg, #2a2a72 0%, #009ffd 74%)",
                WebkitBackgroundClip:"text",
                WebkitTextFillColor:"transparent",
                fontWeight:"800",
                fontFamily: "'Lobster', cursive"
              }}>{item.label}</p>
          </li>
          </Link>
        </Fragment>
    );
    return (
      list
    );
  }
}

export default MenuItemList
