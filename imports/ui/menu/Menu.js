import React, { Component } from 'react'
import MenuItemList from './MenuItemList';
import { Link } from 'react-router-dom';
import { Button,Icon } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';

class Menu extends Component {

  state={
    menuItems:[
      {
        name:"ingredients",
        label:"Ingredients"
      },{
        name:"recettes",
        label:"Recettes"
      },{
        name:"clients",
        label:"Clients"
      },{
        name:"prestations/"+new Date().getFullYear()+"/"+parseInt(new Date().getMonth()+1),
        label:"Prestations"
      },{
        name:"parametres",
        label:"Parametres"
      }
    ]
  }

  render() {
    const { menuItems } = this.state;
    return (
      <div style={{
        width:"180px",
        height:"100vh",
        position:"fixed",
        display:"inline-block",
        backgroundImage:"linear-gradient(315deg, #ffffff 0%, #d7e1ec 74%)",
        boxShadow:"1px 0 5px black"
      }}>
          <ul style={{padding:"0",listStyle:"none",textAlign:"center"}}>
            <Link to={"/"} style={{ textDecoration: 'none' }}>
              <li style={{cursor:"pointer"}} name="avatarWrapper">
                <img alt="homeLogo" style={{marginTop:"16px",width:"108px",height:"108px"}} src='/res/cake.png'/>
                <p style={{
                  margin:"16px 0 32px 0",
                  fontSize:"1.7em",
                  backgroundColor:"#2a2a72",
                  backgroundImage: "linear-gradient(315deg, #2a2a72 0%, #009ffd 74%)",
                  WebkitBackgroundClip:"text",
                  WebkitTextFillColor:"transparent",
                  fontWeight:"700",
                  fontFamily: "'Lobster', cursive"
                }}>Ecrin de Gourmandises</p>
              </li>
            </Link>
            <MenuItemList menuItems={menuItems}/>
            <Button inverted style={{width:"128px",marginTop:"32px"}} onClick={()=>{Meteor.logout();this.props.client.resetStore();}} color="red" animated='fade'>
              <Button.Content visible><Icon name='cancel' /></Button.Content>
              <Button.Content hidden>Déconnexion</Button.Content>
            </Button>
          </ul>
      </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(Menu);