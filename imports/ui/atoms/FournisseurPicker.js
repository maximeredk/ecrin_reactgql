import React, { Component } from 'react';
import { Dropdown } from 'semantic-ui-react';

export default class FournisseurPicker extends Component {

    state={
        fournisseurs:[
            {key:"none",text:"Aucun",value:"",color:"#fff"},
            {key:"Pro à pro",text:"Pro à pro",value:"Pro à pro",color:"#2ecc71"},
            {key:"GMD",text:"GMD",value:"GMD",color:"#3498db"},
            {key:"Patisfrance",text:"Patisfrance",value:"Patisfrance",color:"#9b59b6"},
            {key:"Pedrero",text:"Pedrero",value:"Pedrero",color:"#f1c40f"},
            {key:"Coup de pâte",text:"Coup de pâte",value:"Coup de pâte",color:"#e67e22"},
            {key:"Thecakedecorating",text:"Thecakedecorating",value:"Thecakedecorating",color:"#e74c3c"},
            {key:"Planète gâteau",text:"Planète gâteau",value:"Planète gâteau",color:"#1abc9c"},
            {key:"Autour du gâteau",text:"Autour du gâteau",value:"Autour du gâteau",color:"#34495e"}
        ]
    }

    selectFournisseur = (e, { value }) => this.props.changeFournisseur(value)
    
    render() {
        return (
            <Dropdown defaultValue={this.props.fournisseur} onChange={this.selectFournisseur} name="unite" options={this.state.fournisseurs} />
        )
    }
}
