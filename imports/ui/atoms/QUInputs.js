import React, { Component,Fragment } from 'react';
import { Dropdown,Table,Input } from 'semantic-ui-react';

export class QUInputs extends Component {

    state={
        quantite:this.props.quantite,
        unite:this.props.unite,
        units : this.props.units
    }

    selectUnit = (e,{value}) => {
        this.props.handleChange(
            {target:
                {
                    value:value,
                    name:"newUsedU"
                }
            }
        )
    }

  render() {
    const { quantite,unite,units } = this.state;
    return (
        <Fragment>
            <Table.Cell textAlign='center'>
                <div style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
                    <Input label={
                        <Dropdown defaultValue={unite} onChange={this.selectUnit} name="newUsedU" options={units} />
                    }
                    defaultValue={quantite} labelPosition='right' name="newUsedQ" onChange={this.props.handleChange} placeholder='Quantité'/>
                </div>
            </Table.Cell>
        </Fragment>
    )
  }
}

export default QUInputs
