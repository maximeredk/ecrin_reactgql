import React, { Component } from 'react'
import { Button } from 'semantic-ui-react';

export class ConfirmButton extends Component {

    state={
        content:this.props.content,
        action:this.props.action,
        color:this.props.color
    }

    handleClick = () => {
        
    }

  render() {
    return (
      <Button onClick={()=>{this.handleClick()}}>
        {this.state.content}
      </Button>
    )
  }
}

export default ConfirmButton
