import React, { Component,Fragment } from 'react';
import { Dropdown,Table,Input } from 'semantic-ui-react';

export class QUPInputs extends Component {

    state={
        prix:this.props.prix,
        quantite:this.props.quantite,
        unite:this.props.unite,
        units:this.props.units
    }

    selectUnit = (e,{value}) => {
        this.props.handleChange(
            {target:
                {
                    value:value,
                    name:"unite"
                }
            }
        )
    }

  render() {
    const { quantite,prix,unite,units } = this.state;
    return (
        <Fragment>
            <Table.Cell textAlign='center'>
                <Input placeholder='Prix' defaultValue={prix} name='prix' label={{ basic: false, content: '€' }} labelPosition='right' onChange={this.props.handleChange}/>
            </Table.Cell>
            <Table.Cell textAlign='center'>
                <div style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
                    <Input label={
                        <Dropdown defaultValue={unite} onChange={this.selectUnit} name="unite" options={units} />
                    }
                    defaultValue={quantite} labelPosition='right' name="quantite" onChange={this.props.handleChange} placeholder='Quantité'/>
                </div>
            </Table.Cell>
        </Fragment>
    )
  }
}

export default QUPInputs
