import '../imports/startup/server';
import { Accounts } from "meteor/accounts-base";
import { Meteor } from 'meteor/meteor';

Accounts.onCreateUser(function(options, user) {
    
    // Use provided profile in options, or create an empty object
    user.profile = options.profile || {};
    user.profile.firstName = "";
    user.profile.lastName = "";
    user.profile.age = 0;

    user.settings = options.settings || {};

    if(Meteor.users.find().count() == 0){
        user.settings.isAdmin = true;
        user.settings.activated = true;
    }else{
        user.settings.isAdmin = false;
        user.settings.activated = false;
    }

    user.favorites = [];

    // Returns the user object
    return user;
 });